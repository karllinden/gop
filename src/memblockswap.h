/*
 * This file is part of gop.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef MEMBLOCKSWAP_H
# define MEMBLOCKSWAP_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <stddef.h>

/**
 * Swap the first and last part of a memory block.
 *
 * The following example prints:
 *  cdef
 *  abcdef
 *
 * void example() {
 *     char str[] = "abcdef";
 *     memblockswap(str, 2, 5);
 *     puts(str);
 *     memblockswap(str, 5, 2);
 *     puts(str);
 * }
 *
 * @param mem a pointer to the memory block
 * @param first_size the size of the first memory block
 * @param second_size the size of the second memory block
 */
void memblockswap(void * mem, size_t first_size, size_t second_size)
    __attribute__((nonnull));

#endif /* !MEMBLOCKSWAP_H */
