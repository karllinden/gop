/*
 * This file is part of gop.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stddef.h>

#include "memblockswap.h"

/**
 * Swap the content of two memory blocks of a given size.
 *
 * The blocks may not overlap.
 *
 * @param mem1 a pointer to the first memory block
 * @param mem2 a pointer to the second memory block
 * @param size the size of the memory blocks
 */
static void __attribute__((nonnull))
memswap(void * mem1, void * mem2, size_t size)
{
    char * char1 = mem1;
    char * char2 = mem2;
    char tmp;

    while (size--) {
        tmp = *char1;
        *char1 = *char2;
        *char2 = tmp;
        char1++;
        char2++;
    }

    return;
}

void __attribute__((nonnull))
memblockswap(void * mem, size_t first_size, size_t second_size)
{
    char * base = mem;

    while (first_size && second_size) {
        if (first_size <= second_size) {
            memswap(base, base + first_size, first_size);
            second_size -= first_size;
            base += first_size;
        } else {
            memswap(base, base + first_size, second_size);
            first_size -= second_size;
            base += second_size;
        }
    }

    return;
}
