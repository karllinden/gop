/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#if HAVE_TIOCGWINSZ
# include <sys/ioctl.h>
#endif /* HAVE_TIOCGWINSZ */

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#if ENABLE_NLS
# include <wchar.h>
#endif /* ENABLE_NLS */

#include <gop.h>

#include "error.h"
#include "exit.h"
#include "expect.h"
#include "internal.h"
#include "nls.h"
#include "program-name.h"

#if !HAVE_FLOCKFILE || !HAVE_FUNLOCKFILE
# define flockfile(stream) /* empty */
# define funlockfile(stream) /* empty */
# define fileno_unlocked(stream) fileno(stream)
# define fputc_unlocked(c, stream) fputc(c, stream)
# define fputs_unlocked(s, stream) fputs(s, stream)
#else /* HAVE_FLOCKFILE && HAVE_FUNLOCKFILE */
# if !HAVE_FILENO_UNLOCKED
#  define fileno_unlocked(stream) fileno(stream)
# endif /* !HAVE_FILENO_UNLOCKED */
# if !HAVE_FPUTC_UNLOCKED
#  define fputc_unlocked(c, stream) fputc(c, stream)
# endif /* !HAVE_FPUTC_UNLOCKED */
# if !HAVE_FPUTS_UNLOCKED
#  define fputs_unlocked(s, stream) fputs(s, stream)
# endif /* !HAVE_FPUTS_UNLOCKED */
#endif /* HAVE_FLOCKFILE && HAVE_FUNLOCKFILE */

/*
 * Through the help and usage printing a distinction between column and width
 * is made. The column is (as the name implies) a column on the terminal (or any
 * other output). The width, however, is the space between two columns. A few
 * things can be noted in this discussion.
 * (1)  The result of a subtraction with two columns is a width.
 * (1') The sum of a column and a width is a column.
 * (2)  Addition of two columns is undefined (this means columns should never be
 *      added in the following code!).
 * (2') Columns cannot be multiplied, not divided.
 * (3)  Widths can be added and subtracted with a width as result.
 * (3') Widths can be divided and multiplied by constants with a width as a
 *      result.
 * Compare this to the relation between points (~columns) and vectors (~width).
 *
 * To emphasize this in code all variables and macros that denote either width
 * or column should be clearly named.
 */

/* the column where the name of the table will be printed */
#define GOP_COL_TABLE 1

/*
 * the column where the options will be printed; not necessarily more than
 * GOP_COL_TABLE but that looks good.
 */
#define GOP_COL_OPTION 2

/* the width of "-a, "; defined for readability */
#define GOP_WIDTH_SHORT 4

/*
 * the minimum and maximum width for long options and argument descriptions,
 * note that the maximum will not honoured if the terminal is wide enough to
 * hold both options (without "squeezing") and descriptions
 */
#define GOP_WIDTH_LONG_MIN 8
#define GOP_WIDTH_LONG_MAX 24

/* the minimum and maximum spacing between long options and descriptions */
#define GOP_WIDTH_SPACING_MIN 2
#define GOP_WIDTH_SPACING_MAX 4

/* the minimum width for descriptions */
#define GOP_WIDTH_DESC_MIN 16
#define GOP_WIDTH_DESC_MAX 48

/*
 * The maximum with gop_pretty_print() will handle. This is set to make sure
 * that text which should be readable is not printed with a too big width, which
 * makes the text uncomfortable to read. 72 is a decent default.
 */
#define GOP_WIDTH_PRETTY_PRINT_MAX 72

/* the minimum acceptable terminal width */
#define GOP_TERMWIDTH_MIN (GOP_COL_OPTION + GOP_WIDTH_SHORT + \
                           GOP_WIDTH_LONG_MIN +  GOP_WIDTH_SPACING_MIN + \
                           GOP_WIDTH_DESC_MIN)

#if ENABLE_NLS
# define T_(table, string) (dgettext(table->domain, string))
#else /* !ENABLE_NLS */
# define T_(table, string) (string)
#endif /* !ENABLE_NLS */

/* This is the default argument description for all yesno options. */
#define GOP_YESNO_ARG_DESC "no|yes"

#define GOP_TERMWIDTH_DEFAULT 80

struct gop_width_loop_s {
    const char * string;
    const char * break_chars;
    unsigned     width;
    unsigned     col;
};
typedef struct gop_width_loop_s gop_width_loop_t;

static const gop_option_t gop_autohelp_table[] = {
    {"help", '?', GOP_NONE, NULL, &gop_print_help, N_("Show this help message"),
        NULL},
    {"usage", '\0', GOP_NONE, NULL, &gop_print_usage,
        N_("Display brief usage message"), NULL},
    GOP_TABLEEND
};

static void * __attribute__((nonnull))
gop_print_buf(gop_t * gop, const char * fmt, va_list ap)
{
    va_list apc;
    char *  buf;
    size_t  size;
    int     ret;

    va_copy(apc, ap);
    ret = vsnprintf(NULL, 0, fmt, apc);
    va_end(apc);

    /*
     * POSIX.1-2008 mandates that when vsprintf fails it sets errno.
     */
    if (ret < 0) {
        gop_set_error(gop, GOP_ERROR_PRINT, true);
        gop_emit_error(gop);
        return NULL;
    }

    size = (size_t)ret + 1;
    buf = (*gop->malloc)(size);
    if (!buf) {
        gop_set_error(gop, GOP_ERROR_PRINT, true);
        gop_emit_error(gop);
        return NULL;
    }

    ret = vsnprintf(buf, size, fmt, ap);
    if (ret < 0) {
        (*gop->free)(buf);
        gop_set_error(gop, GOP_ERROR_PRINT, true);
        gop_emit_error(gop);
        return NULL;
    }

    return buf;
}

int __attribute__((nonnull))
gop_autohelp(gop_t * gop)
{
#if ENABLE_NLS
    return gop_add_table_with_domain(gop, _("Help options:"),
                                     gop_autohelp_table, PACKAGE_DOMAIN);
#else /* !ENABLE_NLS */
    return gop_add_table(gop, _("Help options:"), gop_autohelp_table);
#endif /* !ENABLE_NLS */
}

int __attribute__((nonnull))
gop_add_usage(gop_t * gop, const char * usage)
{
    const char ** usages;

    usages = (*gop->realloc)(gop->usages,
                             (gop->usages_size + 1) * sizeof(char *));
    if (!usages) {
        gop_set_error(gop, GOP_ERROR_NOMEM, true);
        gop_emit_error(gop);
        return 1;
    }

    gop->usages = usages;
    gop->usages[gop->usages_size++] = usage;

    return 0;
}

int __attribute__((format(printf, 2, 3), nonnull))
gop_description(gop_t * gop, const char * fmt, ...)
{
    va_list ap;

    (*gop->free)(gop->description);

    va_start(ap, fmt);
    gop->description = gop_print_buf(gop, fmt, ap);
    va_end(ap);

    return gop->description ? 0 : 1;
}

int __attribute__((format(printf, 2, 3), nonnull))
gop_extra_help(gop_t * gop, const char * fmt, ...)
{
    va_list ap;

    (*gop->free)(gop->extra_help);

    va_start(ap, fmt);
    gop->extra_help = gop_print_buf(gop, fmt, ap);
    va_end(ap);

    return gop->extra_help ? 0 : 1;
}

/* This function does not return 0 or UINT_MAX. */
static unsigned __attribute__((nonnull))
gop_get_termwidth(gop_t * gop)
{
    const char *   columns;
#if HAVE_TIOCGWINSZ
    int            fd;
#endif /* HAVE_TIOCGWINSZ */


    if (gop->termwidth != 0) {
        goto end;
    }

    columns = getenv("COLUMNS");
    if (columns != NULL && *columns != '\0') {
        char *        endptr;
        unsigned long ret;

        errno = 0;
        ret = strtoul(columns, &endptr, 10);
        if (likely(errno == 0 && *endptr == '\0')) {
            if (likely(ret < UINT_MAX)) {
                gop->termwidth = (unsigned)ret;
            } else {
                gop->termwidth = UINT_MAX - 1;
            }
            goto end;
        }
    }

#if HAVE_TIOCGWINSZ
    fd = fileno_unlocked(gop->outfile);
    if (fd >= 0) {
        struct winsize window_size;
        if (ioctl(fd, TIOCGWINSZ, &window_size) == 0) {
            gop->termwidth = window_size.ws_col;
            goto end;
        }
    }
#endif /* HAVE_TIOCGWINSZ */

    gop->termwidth = GOP_TERMWIDTH_DEFAULT;

end:
    return gop->termwidth;
}

void __attribute__((nonnull))
gop_set_termwidth(gop_t * gop, unsigned int termwidth)
{
    gop->termwidth = termwidth;
    return;
}

static void __attribute__((nonnull))
gop_print_spacing(FILE * file, unsigned spacing)
{
    while (spacing-- > 0) {
        fputc_unlocked(' ', file);
    }
    return;
}

static unsigned __attribute__((nonnull))
gop_print_string(gop_t * gop,
                 const char * string,
                 unsigned col,
                 unsigned indent,
                 unsigned width)
{
    unsigned len;
    unsigned max;

    assert(col <= width);
    assert(indent < width);

    len = (unsigned)strlen(string);
    while ((max = width - col) < len) {
        len -= max;
        while (max-- > 0) {
            fputc_unlocked(*string, gop->outfile);
            string++;
        }
        fputc_unlocked('\n', gop->outfile);
        gop_print_spacing(gop->outfile, indent);
        col = indent;
    }
    fputs_unlocked(string, gop->outfile);

    return col + len;
}

#if ENABLE_NLS
static int __attribute__((nonnull))
gop_width_loop(gop_t * gop, gop_width_loop_t * loop)
{
    mbstate_t    mbstate;
    size_t       len = strlen(loop->string);

    for (const char * bc = loop->break_chars; *bc != '\0'; ++bc) {
        const char * end;

        end = memchr(loop->string, *bc, len);
        if (end) {
            len = (size_t)(end - loop->string);
        }
    }

    memset(&mbstate, 0, sizeof(mbstate_t));
    while (len > 0) {
        size_t   size;
        wchar_t  wchar;
        int      ret;
        unsigned uret;

        size = mbrtowc(&wchar, loop->string, len, &mbstate);
        if (size == 0) {
            return 0;
        } else if (unlikely(size == (size_t)(-1))) {
            gop_set_error(gop, GOP_ERROR_PRINT, true);
            return -1;
        } else if (unlikely(size == (size_t)(-2))) {
            gop_set_error(gop, GOP_ERROR_PRINT, false);
            return -1;
        }

        ret = wcwidth(wchar);
        if (unlikely(ret < 0)) {
            gop_set_error(gop, GOP_ERROR_PRINT, false);
            return -1;
        }

        uret = (unsigned)ret;
        if (loop->col + uret > loop->width) {
            return 1;
        }

        len -= size;
        loop->string += size;
        loop->col += uret;
    }

    return 0;
}
#endif /* ENABLE_NLS */

#if ENABLE_NLS
static unsigned __attribute__((nonnull))
gop_print_mbstring(gop_t * gop,
                   const char * string,
                   unsigned col,
                   unsigned indent,
                   unsigned width)
{
    gop_width_loop_t loop;
    int              ret;

    loop.string      = string;
    loop.break_chars = "";
    loop.width       = width;
    loop.col         = col;

    while ((ret = gop_width_loop(gop, &loop)) >= 0) {
        if (loop.string == string) {
            /* No progress. The terminal width is insufficient. */
            gop_set_error(gop, GOP_ERROR_TERMWIDTH, false);
            return UINT_MAX;
        }

        for (; string < loop.string; ++string) {
            fputc_unlocked(*string, gop->outfile);
        }

        if (ret == 0) {
            /* Nothing more to print. */
            return loop.col;
        } else /* if (ret > 0) */ {
            /* More to print. */
            fputc_unlocked('\n', gop->outfile);
            gop_print_spacing(gop->outfile, indent);
            loop.col = indent;
        }
    }

    /* ret < 0 means error. */

    return UINT_MAX;
}
#else /* !ENABLE_NLS */
# define gop_print_mbstring gop_print_string
#endif /* !ENABLE_NLS */

/*
 * This function or macro returns 0 on error. Thus the string must be non-empty.
 */
#if ENABLE_NLS
static unsigned __attribute__((nonnull))
gop_string_width(gop_t * gop, const char * string)
{
    gop_width_loop_t loop;
    int              ret;

    loop.string      = string;
    loop.break_chars = "";
    loop.width       = UINT_MAX;
    loop.col         = 0;

    ret = gop_width_loop(gop, &loop);
    if (unlikely(ret < 0)) {
        /* error */
        return 0;
    } else if (unlikely(ret > 0)) {
        /* very long string */
        gop_set_error(gop, GOP_ERROR_PRINT, false);
        return 0;
    }

    return loop.col;
}
#else /* !ENABLE_NLS */
# define gop_string_width(gop, str) ((unsigned)strlen(str))
#endif /* !ENABLE_NLS */

static unsigned __attribute__((nonnull))
gop_print_help_preamble_usage(gop_t * gop,
                              const char * str,
                              unsigned col,
                              unsigned indent,
                              unsigned width)
{
    /* Respect a deliberately empty string. */
    if (*str != '\0') {
        if (col == width) {
            fputc_unlocked('\n', gop->outfile);
            gop_print_spacing(gop->outfile, indent);
            col = indent;
        } else {
            fputc_unlocked(' ', gop->outfile);
            col++;
        }
        col = gop_print_mbstring(gop, str, col, indent, width);
    }
    return col;
}

/* This function returns 0 on success or 1 otherwise. */
static int __attribute__((nonnull))
gop_print_help_preamble(gop_t * gop, unsigned width)
{
    const char ** usagep;
    const char *  or;
    const char *  usage;
    const char *  default_usage;
    const char *  program_name;
    size_t        size;
    unsigned      or_width;
    unsigned      usage_width;
    unsigned      or_indent;
    unsigned      usage_indent;
    unsigned      indent;
    unsigned      col;

    or = _("or:");
    or_width = gop_string_width(gop, or);
    if (or_width == 0) {
        return 1;
    }

    usage = _("Usage:");
    usage_width = gop_string_width(gop, usage);
    if (usage_width == 0) {
        return 1;
    }

    if (likely(usage_width > or_width)) {
        or_indent = usage_width - or_width - 1;
        usage_indent = 0;
    } else {
        or_indent = 0;
        usage_indent = or_width - usage_width + 1;
    }

    indent = usage_indent + usage_width + 1;
    if (indent + 2 > width) {
        gop_set_error(gop, GOP_ERROR_TERMWIDTH, false);
        return 1;
    }

    default_usage = _("[OPTION...]");
    if (gop->usages_size == 0) {
        usagep = &default_usage;
        size = 1;
    } else {
        usagep = gop->usages;
        size = gop->usages_size;
    }

    program_name = gop_get_program_name(gop);

    gop_print_spacing(gop->outfile, usage_indent);
    fputs_unlocked(usage, gop->outfile);
    fputc_unlocked(' ', gop->outfile);
    col = gop_print_string(gop, program_name, indent, indent + 1, width);
    col = gop_print_help_preamble_usage(gop, *usagep, col, indent + 1, width);
    if (col == UINT_MAX) {
        return 1;
    }
    fputc_unlocked('\n', gop->outfile);

    while (--size > 0) {
        usagep++;
        gop_print_spacing(gop->outfile, or_indent);
        fputs_unlocked(or, gop->outfile);
        fputs_unlocked("  ", gop->outfile);
        col = gop_print_string(gop, program_name, indent, indent + 1, width);
        col = gop_print_help_preamble_usage(gop, *usagep, col, indent + 1,
                                            width);
        if (col == UINT_MAX) {
            return 1;
        }
        fputc_unlocked('\n', gop->outfile);
    }

    return 0;
}

static int __attribute__((nonnull))
gop_next_word(gop_t * gop,
              const char * string,
              size_t * lenp,
              unsigned * widthp)
{
#if ENABLE_NLS
    gop_width_loop_t loop;
    int              ret;

    loop.string      = string;
    loop.break_chars = " \n";
    loop.col         = 0;
    loop.width       = UINT_MAX;

    ret = gop_width_loop(gop, &loop);
    if (unlikely(ret < 0)) {
        /* error */
        return 1;
    } else if (unlikely(ret > 0)) {
        /* very wide word */
        gop_set_error(gop, GOP_ERROR_PRINT, false);
        return 1;
    }

    *lenp = (size_t)(loop.string - string);
    *widthp = loop.col;
    return 0;
#else /* !ENABLE_NLS */
    const char * end;
    unsigned     len;

    end = strpbrk(string, " \n");
    if (end != NULL) {
        len = (unsigned)(end - string);
    } else {
        len = (unsigned)strlen(string);
    }

    *lenp = *widthp = len;
    return 0;
#endif /* !ENABLE_NLS */
}

static int __attribute__((nonnull))
gop_print_wide_word(gop_t * gop,
                    const char ** stringp,
                    unsigned width)
{
#if ENABLE_NLS
    gop_width_loop_t loop;
    int              ret;

    loop.string      = *stringp;
    loop.break_chars = "";
    loop.width       = width;
    loop.col         = 0;

    ret = gop_width_loop(gop, &loop);
    if (unlikely(ret < 0)) {
        /* error */
        return 1;
    }

    for (const char * string = *stringp; string < loop.string; ++string) {
        fputc_unlocked(*string, gop->outfile);
    }
    *stringp = loop.string;

    return 0;
#else /* !ENABLE_NLS */
    const char * string = *stringp;
    while (width--) {
        fputc_unlocked(*string, gop->outfile);
        string++;
    }
    *stringp = string;
    return 0;
#endif /* !ENABLE_NLS */
}

static unsigned __attribute__((nonnull))
gop_pretty_print_line(gop_t * gop,
                      const char ** stringp,
                      unsigned width)
{
    const char * str     = *stringp;
    unsigned     printed = 0;

    while (true) {
        size_t       l;
        unsigned     w;

        if (*str == '\0') {
            goto end;
        } else if (*str == '\n') {
            str++;
            goto end;
        }

        if (printed != 0) {
            str++;
        }

        if (gop_next_word(gop, str, &l, &w)) {
            return UINT_MAX;
        }

        if (printed == 0) {
            if (w > width) {
                /* The word does not fit on any line so line break the word. */
                if (gop_print_wide_word(gop, &str, width)) {
                    return UINT_MAX;
                }
                printed = width;
                goto end;
            }
        } else /* if (printed != 0) */ {
            if (printed + w >= width) {
                /*
                 * A space (to delimit from the previous word) and the word does
                 * not fit on this line.
                 */
                goto end;
            }
            fputc_unlocked(' ', gop->outfile);
            printed++;
        }
        while (l--) {
            fputc_unlocked(*str, gop->outfile);
            ++str;
        }
        printed += w;
    }

end:
    *stringp = str;
    return printed;
}

static int __attribute__((nonnull))
gop_pretty_print(gop_t * gop, const char * string, unsigned width)
{
    if (*string == '\0') {
        fputc_unlocked('\n', gop->outfile);
    }

    if (width > GOP_WIDTH_PRETTY_PRINT_MAX) {
        width = GOP_WIDTH_PRETTY_PRINT_MAX;
    }

    while (*string != '\0') {
        if (gop_pretty_print_line(gop, &string, width) == UINT_MAX) {
            return 1;
        }
        fputc_unlocked('\n', gop->outfile);
    }

    return 0;
}

static int __attribute__((nonnull))
gop_print_help_option(gop_t * gop,
                      const gop_table_t * table,
                      const gop_option_t * option,
                      bool has_short,
                      bool long_overflow,
                      bool desc_overflow,
                      unsigned long_width,
                      unsigned spacing_width,
                      unsigned desc_width)
{
    const char * long_name = "";
    const char * arg_desc = "";
    const char * desc = "";
    unsigned     long_name_width;
    unsigned     arg_desc_width;
    unsigned     this_long_width;
    unsigned     printed;
    unsigned     space;
    bool         has_long;
    bool         has_arg_desc;
    bool         has_desc;
    bool         should_print_equal;


    if (option->long_name != NULL) {
        long_name = option->long_name;
    }

    if (option->argument_description != NULL) {
        arg_desc = T_(table, option->argument_description);
    } else if (option->argument_type == GOP_YESNO) {
        arg_desc = GOP_YESNO_ARG_DESC;
    }

    if (option->description != NULL) {
        desc = T_(table, option->description);
    }

    has_long = (*long_name != '\0');
    has_arg_desc = (*arg_desc != '\0');
    has_desc = (*desc != '\0');

    should_print_equal = (has_long && has_arg_desc);

    long_name_width = (unsigned)strlen(long_name);
    arg_desc_width = 0;
    if (has_arg_desc) {
        arg_desc_width = gop_string_width(gop, arg_desc);
    }
    this_long_width = (has_long ? 2 : 0) + long_name_width +
                           (should_print_equal ? 1 : 0) + arg_desc_width;
    if (long_overflow && this_long_width <= long_width) {
        long_overflow = false;
    }

    gop_print_spacing(gop->outfile, GOP_COL_OPTION);

    if (desc_overflow) {
        unsigned this_desc_width = 0;
        if (*desc != '\0') {
            this_desc_width = gop_string_width(gop, desc);
        }
        if (this_desc_width <= desc_width) {
            desc_overflow = false;
        }
    }

    if (has_short) {
        if (option->short_name != '\0') {
            char buf[5] = {
                '-',
                option->short_name,
                has_long ? ',' : ' ',
                ' ',
                '\0'
            };
            fputs_unlocked(buf, gop->outfile);
        } else {
            fputs_unlocked("    ", gop->outfile);
        }
    }

    if (long_overflow) {
        if (has_long) {
            fputs_unlocked("--", gop->outfile);
            printed = 2;
            if (printed + long_name_width <= long_width) {
                fputs_unlocked(long_name, gop->outfile);
                printed += long_name_width;
                long_name = "";
                if (printed + 1 <= long_width && has_arg_desc)
                {
                    fputc_unlocked('=', gop->outfile);
                    printed++;
                    should_print_equal = false;
                }
            } else {
                if (gop_print_wide_word(gop, &long_name, long_width - 2)) {
                    return 1;
                }
                printed = long_width;
            }
        } else if (has_arg_desc) {
            if (gop_print_wide_word(gop, &arg_desc, long_width)) {
                return 1;
            }
            printed = long_width;
        }
    } else {
        if (has_long) {
            fputs_unlocked("--", gop->outfile);
            fputs_unlocked(long_name, gop->outfile);
            long_name = "";
        }
        if (has_long && has_arg_desc) {
            fputc_unlocked('=', gop->outfile);
        }
        if (has_arg_desc) {
            fputs_unlocked(arg_desc, gop->outfile);
            arg_desc = "";
        }
        printed = this_long_width;
        should_print_equal = false;
    }
    gop_print_spacing(gop->outfile, long_width - printed + spacing_width);

    if (desc_overflow) {
        if (gop_pretty_print_line(gop, &desc, desc_width) == UINT_MAX) {
            return 1;
        }
    } else {
        if (has_desc) {
            fputs_unlocked(desc, gop->outfile);
            desc = "";
        }
    }
    fputc_unlocked('\n', gop->outfile);

    while (*long_name != '\0' || *arg_desc != '\0' || *desc != '\0') {
        assert(long_overflow || desc_overflow);

        space = GOP_COL_OPTION + (has_short ? 4 : 0);

        if (*long_name != '\0' || *arg_desc != '\0') {
            unsigned ln_width;
            unsigned ad_width;
            unsigned left;

            left = long_width;
            if (has_long) {
                space += 2;
                left -= 2;
            }
            gop_print_spacing(gop->outfile, space);
            space = 0;

            ln_width = (unsigned)strlen(long_name);
            ad_width = 0;
            if (*arg_desc != '\0') {
                ad_width = gop_string_width(gop, arg_desc);
            }

            if (ln_width != 0) {
                if (ln_width <= left) {
                    fputs_unlocked(long_name, gop->outfile);
                    long_name = "";
                    left -= ln_width;
                } else {
                    if (gop_print_wide_word(gop, &long_name, left)) {
                        return 1;
                    }
                    left = 0;
                }
            }

            if (should_print_equal && left > 0) {
                fputc_unlocked('=', gop->outfile);
                should_print_equal = false;
                left--;
            }

            if (ad_width != 0) {
                if (ad_width < left) {
                    if (left + 2 == long_width) {
                        /*
                         * Nothing else have been printed on this line so indent
                         * the argument description since it looks fancy.
                         */
                        gop_print_spacing(gop->outfile,
                                          left - ad_width);
                        left = ad_width;
                    }
                    fputs_unlocked(arg_desc, gop->outfile);
                    arg_desc = "";
                    left -= ad_width;
                } else {
                    /*
                     * The + 2 can be hardcoded here without problems, since if
                     * there was no long name and argument description would fit
                     * on one line, this line is not reached; if there was no
                     * option and the argument description would not fit anyway
                     * so adding + 2 is OK; lastly, if there was a long name
                     * the + 2 is the expected.
                     */
                    if (arg_desc_width + 2 < long_width) {
                        /*
                         * Save until the next line so that the argument
                         * description can be indented.
                         */
                        ;
                    } else {
                        gop_print_wide_word(gop, &arg_desc, left);
                        left = 0;
                    }
                }
            }

            space = left;
        } else {
            space += long_width;
        }

        if (*desc != '\0') {
            space += spacing_width + 1;
            gop_print_spacing(gop->outfile, space);
            space = 0;

            if (gop_pretty_print_line(gop, &desc, desc_width - 1) == UINT_MAX) {
                return 1;
            }
        }

        fputc_unlocked('\n', gop->outfile);
    }

    return 0;
}

gop_return_t __attribute__((nonnull))
gop_print_help(gop_t * gop)
{
    int          ret;
    unsigned     width;
    unsigned     long_width = 0;
    unsigned     desc_width = 0;
    unsigned     spacing_width;
    unsigned     left;
    gop_return_t retval     = GOP_DO_EXIT;
    bool         has_short  = false;
    bool         desc_overflow;
    bool         long_overflow;

    flockfile(gop->outfile);

    width = gop_get_termwidth(gop);
    if (width < GOP_TERMWIDTH_MIN) {
        gop_set_error(gop, GOP_ERROR_TERMWIDTH, false);
        goto error;
    }

    ret = gop_print_help_preamble(gop, width);
    if (ret) {
        goto error;
    }

    if (gop->description != NULL) {
        if (gop_pretty_print(gop, gop->description, width)) {
            goto error;
        }
    }

    /*
     * The long option field and the description field are allowed to overflow
     * their formal maximum if the terminal is wide enough to hold both. In the
     * following loop the requested width of all fields will be calculated and
     * later these will be squeezed to fit the terminal width.
     */
    GOP_FOREACH_OPTION(gop, table, opt) {
        const char * arg_desc        = NULL;
        unsigned     this_desc_width = 0;
        unsigned     this_long_width = 0;

        if (opt->short_name != '\0') {
            has_short = true;
        }

        if (opt->argument_description != NULL) {
            arg_desc = opt->argument_description;
        } else if (opt->argument_type == GOP_YESNO) {
            arg_desc = GOP_YESNO_ARG_DESC;
        }

        if (opt->long_name != NULL) {
            this_long_width += 2;
            this_long_width += (unsigned)strlen(opt->long_name);
        }
        if (this_long_width > 0 && arg_desc != NULL) {
            this_long_width++;
        }
        if (arg_desc != NULL) {
            this_long_width += gop_string_width(gop, arg_desc);
        }
        if (this_long_width > long_width) {
            long_width = this_long_width;
        }

        /*
         * It is actually somewhat wrong to calculate the width of the
         * description field since if it is truncated (later on when doing the
         * squeezing to fit the terminal) the size may be off by
         * one or two bytes, but just calculating the description width is fast,
         * simpe and gives a decent result.
         */
        if (opt->description != NULL) {
            this_desc_width = gop_string_width(gop, opt->description);
            if (this_desc_width > desc_width) {
                desc_width = this_desc_width;
            }
        }
    }

    spacing_width = GOP_WIDTH_SPACING_MAX;

    /*
     * Try to squeeze widths together as long as the total calculated width
     * exceeds the total usable width. Firstly the spacing between the tables
     * is reduced. Secondly the width of the description field will be
     * decremented until it reaches it (formal) maximum. Thirdly the same thing
     * is done for the long option field. Lastly the field with the greatest
     * quotient (w-wmin)/(wmax-wmin) where w is the width, wmax and wmin the
     * maximum and minimum width of the field respectively. Note that the
     * description field is shortened first since it looks less ugly to truncate
     * it rather than the long options.
     *
     * The inequality to simplify is the following (l is for long, d is for
     * desc and all variables denote width so any w is omitted):
     *    (l-lmin)/(lmax-lmin) > (d-dmin)/(dmax-dmin)
     * since (lmax-lmin > 0) && (dmax-dmin > 0) both sides can be multiplied
     * with (lmax-lmin)(dmax-dmin) without changing the inequality direction.
     * The result is:
     *    (l-lmin)*(dmax-dmin) > (d-dmin)*(lmax-lmin)
     *                       <=>
     *    l*(dmax-dmin)-lmin*dmax+lmin*dmin > d*(lmax-lmin)-dmin*lmax+dmin*lmin
     *                                 <=>
     *    l*(dmax-dmin) - lmin*dmax > d*(lmax-lmin) - dmin*lmax
     * To avoid negative signs (which may yield a nasty wrap-around) the
     * inequality can be written like this:
     *    l*(dmax-dmin) + dmin*lmax > d*(lmax-lmin) + lmin*dmax
     * which is the form that will be used.
     *
     * Since both sides in the original inequality are non-negative and the
     * window can hold the minimal field widths (that was checked earlier)
     * neither l nor d will be decremented below their respective minimum.
     */
    desc_overflow = false;
    long_overflow = false;
    left = width - GOP_COL_OPTION - (has_short ? 4 : 0);
    while (long_width + spacing_width + desc_width > left) {
        if (spacing_width > GOP_WIDTH_SPACING_MIN) {
            spacing_width--;
        } else if (desc_width > GOP_WIDTH_DESC_MAX) {
            desc_width--;
            desc_overflow = true;
        } else if (long_width > GOP_WIDTH_LONG_MAX) {
            long_width--;
            long_overflow = true;
        } else if (long_width * (GOP_WIDTH_DESC_MAX - GOP_WIDTH_DESC_MIN) +
                     GOP_WIDTH_DESC_MIN * GOP_WIDTH_LONG_MAX >
                   desc_width * (GOP_WIDTH_LONG_MAX - GOP_WIDTH_LONG_MIN) +
                     GOP_WIDTH_LONG_MIN * GOP_WIDTH_DESC_MAX)
        {
            assert(long_width > GOP_WIDTH_LONG_MIN);
            long_width--;
            long_overflow = true;
        } else {
            assert(desc_width > GOP_WIDTH_DESC_MIN);
            desc_width--;
            desc_overflow = true;
        }
    }

    for (size_t i = 0; i < gop->tables_size; ++i) {
        const gop_table_t * table = gop->tables + i;

        fputc_unlocked('\n', gop->outfile);
        if (table->name != NULL) {
            /* the table name is already translated */
            if (gop_pretty_print(gop, table->name, width)) {
                goto error;
            }
        }

        for (const gop_option_t * opt = table->options;
             !is_tableend(opt);
             ++opt)
        {
            if (gop_print_help_option(gop, table, opt, has_short, long_overflow,
                                      desc_overflow, long_width, spacing_width,
                                      desc_width))
            {
                goto error;
            }
        }
    }

    if (gop->extra_help != NULL) {
        fputc_unlocked('\n', gop->outfile);
        if (gop_pretty_print(gop, gop->extra_help, width)) {
            goto error;
        }
    }

    if (false) {
    error:
        /*
         * Make sure the exit status is always EXIT_FAILURE on error.
         * This is needed because this function will never invoke the "normal"
         * error handler gop_emit_error(), because it might exit the program,
         * and the caller function will not invoke the "normal" error handler
         * either because the caller will only see GOP_DO_EXIT or GOP_DO_RETURN
         * from this function.
         */
        gop_set_exit_status(gop, EXIT_FAILURE);

        retval = gop_emit_error_noexit(gop);
        if (retval == GOP_DO_CONTINUE) {
            retval = GOP_DO_EXIT;
        }
    }

    funlockfile(gop->outfile);
    return retval;
}

static unsigned __attribute__((nonnull))
gop_print_usage_preamble(gop_t * gop, unsigned width)
{
    const char * usage        = _("Usage:");
    const char * program_name = gop_get_program_name(gop);
    unsigned     col          = 0;

    col = gop_print_mbstring(gop, usage, col, 0, width);
    if (col == UINT_MAX) {
        return UINT_MAX;
    }

    assert(col <= width);
    if (col == width) {
        fputc_unlocked('\n', gop->outfile);
        col = 0;
    }
    fputc_unlocked(' ', gop->outfile);
    col++;

    return gop_print_string(gop, program_name, col, 0, width);
}

/*
 * This function prints an option with rigorous width checks applicable for tiny
 * terminals.
 */
static unsigned __attribute__((cold, nonnull(1)))
gop_print_usage_option_tiny(gop_t * gop,
                            const char * arg_desc,
                            const char * long_name,
                            char short_name,
                            unsigned col,
                            unsigned indent,
                            unsigned width)
{
    bool has_arg_desc = (arg_desc != NULL);
    bool has_long     = (long_name != NULL);
    bool has_short    = (short_name != '\0');
    unsigned char i   = 0;
    char buf[7];

    buf[i++] = '[';
    if (has_short) {
        buf[i++] = '-';
        buf[i++] = short_name;
    }
    if (has_short && has_long) {
        buf[i++] = '|';
    }
    if (has_long) {
        buf[i++] = '-';
        buf[i++] = '-';
    }
    buf[i] = '\0';
    col = gop_print_string(gop, buf, col, indent, width);

    if (has_long) {
        col = gop_print_string(gop, long_name, col, indent, width);
    }

    if (has_arg_desc) {
        if (col == width) {
            fputc_unlocked('\n', gop->outfile);
            gop_print_spacing(gop->outfile, indent);
            col = indent;
        }
        if (has_long) {
            fputc_unlocked('=', gop->outfile);
        } else /* if (has_short) */ {
            fputc_unlocked(' ', gop->outfile);
        }
        col++;

        col = gop_print_mbstring(gop, arg_desc, col, indent, width);
        if (col == UINT_MAX) {
            return UINT_MAX;
        }
    }

    if (col == width) {
        fputc_unlocked('\n', gop->outfile);
        gop_print_spacing(gop->outfile, indent);
        col = indent;
    }
    fputc_unlocked(']', gop->outfile);
    col++;

    return col;
}

static unsigned __attribute__((nonnull))
gop_print_usage_option(gop_t * gop,
                       const gop_table_t * table,
                       const gop_option_t * opt,
                       unsigned col,
                       unsigned indent,
                       unsigned width)
{
    const char * arg_desc  = NULL;
    unsigned     w;
    bool         has_arg_desc;
    bool         has_long  = (opt->long_name != NULL);
    bool         has_short = (opt->short_name != '\0');

    if (opt->argument_description != NULL) {
        arg_desc = T_(table, opt->argument_description);
    } else if (opt->argument_type == GOP_YESNO) {
        arg_desc = GOP_YESNO_ARG_DESC;
    }
    has_arg_desc = (arg_desc != NULL);

    assert(has_long || has_short);

    /*
     * Options without both long name and argument description must not be
     * printed since the option is in the short list of options.
     */
    if (!has_long && !has_arg_desc) {
        return 0;
    }

    /* w is short for (the occupied) width. 2 for the square brackets */
    w = 2;
    if (has_short) {
        /* for both the hyphen and the short name */
        w += 2;
    }
    if (has_long && has_short) {
        /* for the separator between the long and the short option */
        w += 1;
    }
    if (has_long) {
        /*
         * The long name should not contain any characters with width other than
         * one.
         */
        w += (unsigned)strlen(opt->long_name) + 2;
    }
    if (has_arg_desc) {
        unsigned tmp = gop_string_width(gop, arg_desc);
        if (tmp == 0) {
            return UINT_MAX;
        }
        /* plus one for the separator */
        w += tmp + 1;
    }

    assert(indent < width);
    if (w + indent > width) {
        fputc_unlocked('\n', gop->outfile);
        gop_print_spacing(gop->outfile, indent);
        col = gop_print_usage_option_tiny(gop, arg_desc, opt->long_name,
                                          opt->short_name, indent, indent + 1,
                                          width);
    } else {
        if (col + w + 1 > width) {
            fputc_unlocked('\n', gop->outfile);
            gop_print_spacing(gop->outfile, indent);
            col = indent;
        } else {
            col++;
            fputc_unlocked(' ', gop->outfile);
        }
        col += w;

        fputc_unlocked('[', gop->outfile);
        if (has_short) {
            fputc_unlocked('-', gop->outfile);
            fputc_unlocked(opt->short_name, gop->outfile);
        }
        if (has_short && has_long) {
            fputc_unlocked('|', gop->outfile);
        }
        if (has_long) {
            fputs_unlocked("--", gop->outfile);
            fputs_unlocked(opt->long_name, gop->outfile);
        }
        if (has_arg_desc) {
            if (has_long) {
                fputc_unlocked('=', gop->outfile);
            } else /* if (has_short) */ {
                fputc_unlocked(' ', gop->outfile);
            }
            fputs_unlocked(arg_desc, gop->outfile);
        }
        fputc_unlocked(']', gop->outfile);
    }

    return col;
}

#define GOP_PRINT_USAGE_INDENT 4
gop_return_t __attribute__((nonnull))
gop_print_usage(gop_t * gop)
{
    gop_return_t retval = GOP_DO_EXIT;
    unsigned     width  = gop_get_termwidth(gop);
    unsigned     col;
    bool         has_short_options = false;
    char         short_options[CHAR_MAX + 1];

    flockfile(gop->outfile);

    /*
     * Prints on the form:
     *
     * $ program --usage
     * Usage: program [-acdef...?] [-a|--a-option]
     *     [-b|--b-option=<b-argument>] [-c <c-argument>]....
     *
     * The indentation on the second and subsequent lines is determined
     * by GOP_PRINT_USAGE_INDENT.
     */

    if (width <= GOP_PRINT_USAGE_INDENT + 1) {
        gop_set_error(gop, GOP_ERROR_TERMWIDTH, false);
        goto error;
    }

    col = gop_print_usage_preamble(gop, width);
    if (col == UINT_MAX) {
        goto error;
    }

    memset(short_options, 0, CHAR_MAX + 1);

    GOP_FOREACH_OPTION(gop, table, opt) {
        if (opt->short_name != '\0' && opt->argument_type == GOP_NONE) {
            has_short_options = true;
            assert(opt->short_name >= 0);
            short_options[(unsigned char)opt->short_name] = 1;
        }
    }

    /* [-abcdef...?] */
    if (has_short_options) {
        unsigned i = 0;
        bool     has_question_mark;
        char     buffer[CHAR_MAX + 5];

        buffer[i++] = '[';
        buffer[i++] = '-';

        /* The question mark should always come last. */
        for (unsigned char c = 0; c <= CHAR_MAX; ++c) {
            if (short_options[c] == 1) {
                if (c != '?') buffer[i++] = (char)c;
                else has_question_mark = true;
            }
        }
        if (has_question_mark) {
            buffer[i++] = '?';
        }

        buffer[i++] = ']';
        buffer[i] = '\0';

        if (GOP_PRINT_USAGE_INDENT + i > width) {
            col = gop_print_string(gop, buffer, col, GOP_PRINT_USAGE_INDENT,
                                   width);
        } else {
            if (col + i + 1 > width) {
                fputc_unlocked('\n', gop->outfile);
                gop_print_spacing(gop->outfile, GOP_PRINT_USAGE_INDENT);
                col = GOP_PRINT_USAGE_INDENT;
            } else {
                col++;
                fputc_unlocked(' ', gop->outfile);
            }
            col += i;
            fputs_unlocked(buffer, gop->outfile);
        }
    }

    GOP_FOREACH_OPTION(gop, table, option) {
        col = gop_print_usage_option(gop, table, option, col,
                                     GOP_PRINT_USAGE_INDENT, width);
        if (col == UINT_MAX) {
            goto error;
        }
    }

    fputc_unlocked('\n', gop->outfile);

    if (false) {
    error:
        /*
         * Make sure the exit status is always EXIT_FAILURE on error. This is
         * needed because this function will never invoke the "normal" error
         * handler gop_emit_error(), because it might exit the program, and the
         * caller function will not invoke the "normal" error handler either
         * because the caller will only see GOP_DO_EXIT or GOP_DO_RETURN from
         * this function.
         */
        gop_set_exit_status(gop, EXIT_FAILURE);

        gop_return_t ret = gop_emit_error_noexit(gop);
        if (ret != GOP_DO_RETURN) {
            retval = ret;
        }
    }

    funlockfile(gop->outfile);
    return retval;
}
