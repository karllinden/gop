/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

#include <gop.h>

#include "error.h"
#include "exit.h"
#include "expect.h"
#include "internal.h"
#include "memblockswap.h"
#include "program-name.h"

static int __attribute__((nonnull))
strtoi(const char * nptr, char ** endptr)
{
    long int l = strtol(nptr, endptr, 0);
    if (l > INT_MAX) {
        errno = ERANGE;
        return INT_MAX;
    } else if (l < INT_MIN) {
        errno = ERANGE;
        return INT_MIN;
    } else {
        return (int)l;
    }
}

static unsigned int __attribute__((nonnull))
strtoui(const char * nptr, char ** endptr)
{
    unsigned long int ul = strtoul(nptr, endptr, 0);
    if (ul > UINT_MAX) {
        errno = ERANGE;
        return UINT_MAX;
    } else {
        return (unsigned int)ul;
    }
}

static short __attribute__((nonnull))
strtoh(const char * nptr, char ** endptr)
{
    long int l = strtol(nptr, endptr, 0);
    if (l > SHRT_MAX) {
        errno = ERANGE;
        return SHRT_MAX;
    } else if (l < SHRT_MIN) {
        errno = ERANGE;
        return SHRT_MIN;
    } else {
        return (short)l;
    }
}

static unsigned short __attribute__((nonnull))
strtouh(const char * nptr, char ** endptr)
{
    unsigned long int ul = strtoul(nptr, endptr, 0);
    if (ul > USHRT_MAX) {
        errno = ERANGE;
        return USHRT_MAX;
    } else {
        return (unsigned short)ul;
    }
}

/*
 * This function will fill in the number from the argument expected by the
 * option.
 * option->argument is not allowed to be GOP_NONE, GOP_STRING or GOP_YESNO.
 * Returns 0 on success or 1 on error.
 */
static int __attribute__((nonnull))
fill_in_number_argument(gop_t * gop,
                        const gop_option_t * option,
                        const char * restrict argument)
{
    void * ptr = option->argument;
    char * endptr;

    /*
     * All of these functions will set errno to an appropriate value on error,
     * which is the only way an overflow or underflow can be detected.
     */
    errno = 0;

    switch (option->argument_type) {
        /*
         * Integers that cannot be converted using standard library functions
         * without the risk of overflow. In this case custom written functions
         * have to be used.
         */
        case GOP_INT:
            *(int *)ptr = strtoi(argument, &endptr);
            break;
        case GOP_SHORT:
            *(short *)ptr = strtoh(argument, &endptr);
            break;
        case GOP_UNSIGNED_INT:
            *(unsigned int *)ptr = strtoui(argument, &endptr);
            break;
        case GOP_UNSIGNED_SHORT:
            *(unsigned short *)ptr = strtouh(argument, &endptr);
            break;

        /*
         * Integers that can be converted by directly using an existing library
         * function, without risk of overflow.
         */
        case GOP_LONG_INT:
            *(long int *)ptr = strtol(argument, &endptr, 0);
            break;
        case GOP_LONG_LONG_INT:
            *(long long int *)ptr = strtoll(argument, &endptr, 0);
            break;
        case GOP_UNSIGNED_LONG_INT:
            *(unsigned long int *)ptr = strtoul(argument, &endptr, 0);
            break;
        case GOP_UNSIGNED_LONG_LONG_INT:
            *(unsigned long long int *)ptr = strtoull(argument, &endptr, 0);
            break;

        /* Floating point numbers. */
        case GOP_FLOAT:
            *(float *)ptr = strtof(argument, &endptr);
            break;
        case GOP_DOUBLE:
            *(double *)ptr = strtod(argument, &endptr);
            break;
        case GOP_LONG_DOUBLE:
            *(long double *)ptr = strtold(argument, &endptr);
            break;

        /* Unknown type. An error. */
        default:
            gop_set_error(gop, GOP_ERROR_UNKTYPE, false);
            return 1;
    }

    /*
     * Find errors. Require that the entire argument was converted and that no
     * overflow or underflow error occurred.
     */
    if (*endptr != '\0' || errno != 0) {
        gop_set_error(gop, GOP_ERROR_CONV, (errno != 0));
        gop->error_data.conv.option = option;
        gop->error_data.conv.argument = argument;

        return 1;
    }

    return 0;
}

/**
 * Destruct an argv entry.
 *
 * @param gop the gop context
 * @param arg the argv entry to destruct
 */
static void __attribute__((nonnull))
destruct_arg(gop_t * gop, char * arg)
{
    if (gop->arg_destructor) {
        (*gop->arg_destructor)(arg);
    }
}

/*
 * This function will fill in the pointer with the properly parsed data.
 * option->argument is not allowed to be GOP_NONE.
 * Returns 0 on success or 1 on error.
 */
static int __attribute__((nonnull))
fill_in_argument(gop_t * gop,
                 const gop_option_t * option,
                 char * restrict argument)
{
    char ** stringp;
    int *   intp;

    switch (option->argument_type) {
        case GOP_STRING:
            stringp = option->argument;
            if (*stringp != NULL) {
                destruct_arg(gop, *stringp);
            }
            *stringp = argument;
            return 0;
        case GOP_YESNO:
            intp = option->argument;
            if (strcmp(argument, "yes") == 0) {
                *intp = 1;
            } else if (strcmp(argument, "no") == 0) {
                *intp = 0;
            } else {
                gop_set_error(gop, GOP_ERROR_YESNO, false);
                gop->error_data.yesno.option = option;
                gop->error_data.yesno.argument = argument;
                return 1;
            }
            return 0;
        default:
            return fill_in_number_argument(gop, option, argument);
    }

    return 0;
}

/*
 * Calls the callback associated with the option (if any). This function cannot
 * return an invalid return value.
 */
static gop_return_t __attribute__((nonnull))
call_callback(gop_t * gop,
              const gop_option_t * option)
{
    if (option->callback == NULL) {
        return GOP_DO_CONTINUE;
    } else {
        gop_return_t ret = (option->callback)(gop);
        switch (ret) {
            case GOP_DO_CONTINUE:
            case GOP_DO_EXIT:
            case GOP_DO_RETURN:
                return ret;
            default:
                gop_set_error(gop, GOP_ERROR_INVRET, false);
                gop->error_data.invret.value = ret;
                return GOP_DO_ERROR;
        }
    }
}

static gop_return_t __attribute__((nonnull))
found_none_option(gop_t * gop, const gop_option_t * option)
{
    int * ptr = option->argument;
    if (ptr != NULL) {
        (*ptr)++;
    }
    return call_callback(gop, option);
}

static gop_return_t __attribute__((nonnull))
found_other_option(gop_t * gop,
                   const gop_option_t * option,
                   char * argument)
{
    if (fill_in_argument(gop, option, argument)) {
        return GOP_DO_ERROR;
    }
    return call_callback(gop, option);
}

/**
 * Handle an option that is not a GOP_NONE option.
 *
 * This function takes care of consuming the relevant argv entries on success,
 * so no attempt must be made to access those after a successful call to this
 * function.
 *
 * @param gop the context in which to handle the option
 * @param option the option to handle
 * @param argv the argument vector
 * @param argip the pointer to the current argument index
 * @param argument the argument found in the same argv entry, or NULL if no such
 *                 argument exists
 */
static gop_return_t __attribute__((nonnull(1,2,3,4)))
handle_other_option(gop_t * gop,
                    const gop_option_t * option,
                    char ** argv,
                    int * argip,
                    char * argument)
{
    char *       arg           = argv[*argip];
    char *       next          = argv[*argip + 1];
    size_t       base_size     = 0;
    size_t       argument_size = 0;
    gop_return_t ret;

    if (argument == NULL && next == NULL) {
        gop_set_error(gop, GOP_ERROR_EXPARG, false);
        gop->error_data.exparg.option = option;
        return GOP_DO_ERROR;
    }

    if (option->argument_type == GOP_STRING && argument != NULL) {
        /*
         * The argument must be moved to the beginning of the current argv entry
         * so that it can be properly destructed.
         */
        base_size = (size_t)(argument - arg);
        argument_size = strlen(argument) + 1;
        memblockswap(arg, base_size, argument_size);
        argument = arg;
    }

    ret = found_other_option(gop,
                             option,
                             argument != NULL ? argument : next);

    if (ret != GOP_DO_ERROR) {
        /*
         * Destruct and consume the argv entry containing the option if the
         * option was given in a separate argv entry.
         */
        if (argument == NULL) {
            destruct_arg(gop, argv[*argip]);
            (*argip)++;
        }

        /*
         * Consume the argv entry containing the argument but only destruct it
         * if the argument has not been passed to the program.
         */
        if (option->argument_type != GOP_STRING) {
            destruct_arg(gop, argv[*argip]);
        }
        (*argip)++;
    } else if (argument_size) {
        /*
         * An error occurred and the argument has been swapped in the argv
         * entry, so the entry must be restored.
         */
        memblockswap(arg, argument_size, base_size);
    }

    return ret;
}

/**
 * Find an option by its long name. This function searches for the given long
 * option in the specified context.
 *
 * @param gop the context in which to find the option
 * @param long_name the long name to search for
 * @return the option if found, or NULL if the option could not be found
 */
static const gop_option_t * __attribute__((nonnull, pure))
find_long_option(const gop_t * gop, const char * long_name)
{
    GOP_FOREACH_OPTION(gop, table, option) {
        if (option->long_name != NULL &&
            strcmp(option->long_name, long_name) == 0) {
            return option;
        }
    }
    return NULL;
}

/*
 * This function will search for the requested short option.
 * Returns a pointer to the short option on if it was found or NULL if it was
 * not found.
 */
static const gop_option_t * __attribute__((nonnull, pure))
find_short_option(const gop_t * gop, char short_name)
{
    GOP_FOREACH_OPTION(gop, table, option) {
        if (option->short_name == short_name) {
            return option;
        }
    }
    return NULL;
}

/*
 * This function will parse a long option.
 * This function will not return an invalid return value.
 */
static gop_return_t __attribute__((nonnull))
parse_long_option(gop_t * gop, char ** argv, int * argip)
{
    char *               arg = argv[*argip];
    char *               long_name = arg + 2;
    char *               argument = strchr(long_name, '=');
    const gop_option_t * option;
    gop_return_t         ret;

    if (argument != NULL) {
        *argument = '\0';
        argument++;
    }

    option = find_long_option(gop, long_name);

    /*
     * Since the option name is not needed anymore, restore the overwritten
     * equal sign if any.
     */
    if (argument != NULL) {
        *(argument - 1) = '=';
    }

    if (option == NULL) {
        gop_set_error(gop, GOP_ERROR_UNKLOPT, false);
        gop->error_data.unklopt.long_option = arg;
        ret = GOP_DO_ERROR;
    } else if (option->argument_type != GOP_NONE) {
        ret = handle_other_option(gop, option, argv, argip, argument);
    } else if (argument == NULL) {
        ret = found_none_option(gop, option);
        if (ret !=  GOP_DO_ERROR) {
            /* Consume the option argument. */
            destruct_arg(gop, arg);
            (*argip)++;
        }
    } else {
        /*
         * An error. Something like --long_name=argument where the option
         * does not take an argument.
         */
        gop_set_error(gop, GOP_ERROR_UNEXARG, false);
        gop->error_data.unexarg.option = option;
        gop->error_data.unexarg.argument = argument;
        ret = GOP_DO_ERROR;
    }

    return ret;
}

/* This function will not return an invalid return value. */
static gop_return_t __attribute__((nonnull))
parse_short_options(gop_t * gop, char ** argv, int * argip)
{
    char *       arg = argv[*argip];
    char *       opts = arg + 1;
    char *       this = opts;
    gop_return_t ret  = GOP_DO_CONTINUE;

    while (*this != '\0' && ret == GOP_DO_CONTINUE) {
        const gop_option_t * option;

        option = find_short_option(gop, *this);
        if (option == NULL) {
            gop_set_error(gop, GOP_ERROR_UNKSOPT, false);
            gop->error_data.unksopt.short_option = *this;
            ret = GOP_DO_ERROR;
        } else if (option->argument_type == GOP_NONE) {
            ret = found_none_option(gop, option);
            if (ret != GOP_DO_ERROR) {
                /* Consume the short option. */
                this++;
                if (*this == '\0') {
                    /*
                     * If all short options have been consumed, also consume the
                     * argv entry.
                     */
                    this = "";
                    destruct_arg(gop, arg);
                    (*argip)++;
                }
            }
        } else {
            char * argument = (this[1] != '\0') ? this + 1 : NULL;
            ret = handle_other_option(gop, option, argv, argip, argument);
            if (ret != GOP_DO_ERROR) {
                /* Make sure the loop stops and prevent the memmove below. */
                this = "";
            }
        }
    }

    if (*this != '\0') {
        /*
         * The argument was not fully parsed overwrite parsed options with the
         * unparsed ones so that the option that has already been parsed here do
         * not occur again.
         */
        memmove(opts, this, strlen(this) + 1);
    }

    return ret;
}

int __attribute__((nonnull))
gop_parse(gop_t * gop, int * argcp, char ** argv)
{
    int          sign     = 1;
    int          argi     = 1;
    gop_return_t ret      = GOP_DO_CONTINUE;

    if (!gop_has_program_name(gop)) {
        const char * sep = strrchr(argv[0], '/');
        const char * name = (sep == NULL) ? argv[0] : sep + 1;
        if (gop_set_program_name(gop, name)) {
            return 0;
        }
    }

    while (argi < *argcp && ret == GOP_DO_CONTINUE) {
        char * arg = argv[argi];

        /*
         * Copy the current argi so that it can be determined whether or not the
         * subroutines have consumed any arguments.
         */
        int new_argi = argi;

        assert(arg != NULL);

        if (arg[0] == '-' && arg[1] == '-' && arg[2] != '\0') {
            ret = parse_long_option(gop, argv, &new_argi);
        } else if (arg[0] == '-' && arg[1] != '\0') {
            ret = parse_short_options(gop, argv, &new_argi);
        }

        /*
         * If any argument has been consumed, update argc and argv before
         * possibly handing over control to the program through a callback.
         */
        if (new_argi != argi) {
            int left = *argcp - new_argi;
            memmove(argv + argi,
                    argv + new_argi,
                    (size_t)(left + 1) * sizeof(char *));
            *argcp = argi + left;
        }

        if (ret == GOP_DO_EXIT || ret == GOP_DO_ERROR) {
            if (ret == GOP_DO_EXIT) {
                ret = gop_exit(gop);
            } else {
                sign = -1;
                ret = gop_emit_error(gop);
            }
        }

        if (new_argi == argi && ret == GOP_DO_CONTINUE) {
            /*
             * The option was not consumed and it is requested to continue.
             * Thus, step to the next argument, leaving the one that was not
             * consumed unchanged. This prevents the loop from continuing
             * forever.
             */
            argi++;
        }
    }

    return sign * argi;
}
