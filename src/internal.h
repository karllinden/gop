/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* internal.h - library internals */

#ifndef INTERNAL_H
# define INTERNAL_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <stdbool.h>
# include <stddef.h>
# include <stdio.h>

# include <gop.h>

/* A macro that determines whether option is a table end or not. */
# define is_tableend(option) (option->argument_type == GOP_END)

# define GOP_FOREACH_OPTION(gop, table, option) \
    for (const gop_table_t * table = gop->tables; \
         table < gop->tables + gop->tables_size; \
         ++table) \
        for (const gop_option_t * option = table->options; \
             !is_tableend(option); \
             ++option)

struct gop_table_s {
# if ENABLE_NLS
    const char * domain;
# endif /* ENABLE_NLS */
    const char * name;
    const gop_option_t * options;
};
typedef struct gop_table_s gop_table_t;

struct gop_s {
    size_t tables_size;
    gop_table_t * tables;

    FILE * errfile;
    FILE * outfile;

    gop_malloc_callback_t * malloc;
    gop_free_callback_t * free;
    gop_realloc_callback_t * realloc;
    gop_strdup_callback_t * strdup;

    unsigned int termwidth;

    gop_arg_destructor_t * arg_destructor;

    /*
     * A pointer that the library user can set or get with the gop_set_pointer
     * and gop_get_pointer functions.
     */
    void * pointer;

    char * program_name;

    /*
     * This will be printed before the option tables, directly after the usage.
     * It is set using gop_description().
     */
    char * description;

    /*
     * This is the extra help that will be printed after the standard --help
     * output.
     */
    char * extra_help;

    /*
     * This is the string that can be printed instead of "[OPTION...]" in
     * "Usage: program [OPTION...]". If it is set to NULL "[OPTION...]" will be
     * printed.
     */
    size_t usages_size;
    const char ** usages;

    /*
     * A function to be called when gop calls exit() (after finding a --help
     * argument for example).
     */
    gop_callback_t * atexit_callback;

    /*
     * If gop_exit() is called this status will be sent to exit(). The default
     * value is EXIT_SUCCESS if no error was encountered and EXIT_FAILURE if an
     * error was encountered. It can be set and get with
     * gop_{get,set}_exit_status().
     */
    int exit_status;

    /* This is a function that will be called when an error occured. */
    gop_callback_t * error_callback;

    /* This is a variable containing the error. */
    gop_error_t error;

    /*
     * This is the additional error data structure used to store extra
     * information about the error.
     */
    gop_error_data_t error_data;

    /*
     * This variable is set to true if the current errno varriable is
     * set for the current error.
     */
    bool errno_set;
};

#endif /* !INTERNAL_H */
