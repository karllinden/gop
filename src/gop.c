/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <gop.h>

#include "error.h"
#include "expect.h"
#include "internal.h"
#include "nls.h"

#if NLS
# define CONST_IF_NOT_NLS /* empty */
#else /* !NLS */
# define CONST_IF_NOT_NLS __attribute__((const))
#endif /* NLS */


gop_t *
gop_new_mem(gop_malloc_callback_t * malloc,
            gop_free_callback_t * free,
            gop_realloc_callback_t * realloc,
            gop_strdup_callback_t * strdup)
{
    gop_t * gop = (*malloc)(sizeof(gop_t));
    if (unlikely(gop == NULL)) {
        return NULL;
    }

    gop->tables_size = 0;
    gop->tables = NULL;

    gop->errfile = stderr;
    gop->outfile = stdout;

    gop->malloc = malloc;
    gop->free = free;
    gop->realloc = realloc;
    gop->strdup = strdup;

    gop->termwidth = 0;

    gop->arg_destructor = NULL;

    gop->program_name = NULL;
    gop->description = NULL;
    gop->extra_help = NULL;

    gop->usages_size = 0;
    gop->usages = NULL;

    gop->atexit_callback = NULL;
    gop->exit_status = EXIT_SUCCESS;

    /*
     * Do not set the user settable pointer here as it is not required. If
     * gop_get_pointer() is called without a prior call to gop_set_pointer() the
     * result is undefined.
     */
    /* gop->pointer = NULL; */

    gop->error_callback = NULL;
    gop->error = GOP_ERROR_NONE;
    gop->errno_set = false;

    return gop;
}

gop_t *
gop_new(void)
{
    return gop_new_mem(&malloc, &free, &realloc, &strdup);
}

void
gop_destroy(gop_t * gop)
{
    if (gop == NULL) return;

    (*gop->free)(gop->tables);
    (*gop->free)(gop->program_name);
    (*gop->free)(gop->description);
    (*gop->free)(gop->extra_help);
    (*gop->free)(gop->usages);

    (*gop->free)(gop);
    return;
}

/*
 * Initialize the NLS support in the library if NLS is built in otherwise this
 * is a no-op.
 */
int CONST_IF_NOT_NLS
gop_init_nls(void)
{
#if ENABLE_NLS
    if (bindtextdomain(PACKAGE_DOMAIN, LOCALEDIR) == NULL) {
        return 1;
    }
#endif /* ENABLE_NLS */
    return 0;
}

int __attribute__((nonnull(1,3)))
gop_add_table_with_domain(gop_t * gop,
                          const char * name,
                          const gop_option_t * options,
                          const char * domain)
{
    gop_table_t * tables;
    size_t        tables_size;

    tables_size = gop->tables_size + 1;
    tables = (*gop->realloc)(gop->tables,
                             tables_size * sizeof(gop_table_t));
    if (!tables) {
        gop_set_error(gop, GOP_ERROR_NOMEM, true);
        gop_emit_error(gop);
        return 1;
    }

#if ENABLE_NLS
    tables[tables_size-1].domain = domain;
#endif /* ENABLE_NLS */

    tables[tables_size-1].name = name;
    tables[tables_size-1].options = options;

    gop->tables = tables;
    gop->tables_size = tables_size;

    return 0;
}

int __attribute__((nonnull(1,3)))
gop_add_table(gop_t * gop,
              const char * name,
              const gop_option_t * options)
{
    return gop_add_table_with_domain(gop, name, options, NULL);
}

void __attribute__((nonnull(1)))
gop_set_arg_destructor(gop_t * gop, gop_arg_destructor_t * arg_destructor)
{
    gop->arg_destructor = arg_destructor;
    return;
}

void * __attribute__((nonnull, pure))
gop_get_pointer(const gop_t * gop)
{
    return gop->pointer;
}

void __attribute__((nonnull(1)))
gop_set_pointer(gop_t * gop, void * pointer)
{
    gop->pointer = pointer;
    return;
}

void __attribute__((nonnull))
gop_errfile(gop_t * gop, FILE * errfile)
{
    gop->errfile = errfile;
    return;
}

void __attribute__((nonnull))
gop_outfile(gop_t * gop, FILE * outfile)
{
    gop->outfile = outfile;
    return;
}
