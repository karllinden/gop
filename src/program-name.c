/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <gop.h>

#include "error.h"
#include "expect.h"
#include "internal.h"
#include "program-name.h"

const char * __attribute__((nonnull, pure, returns_nonnull))
gop_get_program_name(const gop_t * gop)
{
    if (gop_has_program_name(gop)) {
        return gop->program_name;
    } else {
        return PACKAGE_NAME;
    }
}

int __attribute__((nonnull(1)))
gop_set_program_name(gop_t * gop, const char * program_name)
{
    (*gop->free)(gop->program_name);
    if (program_name != NULL) {
        gop->program_name = (*gop->strdup)(program_name);
        if (unlikely(gop->program_name == NULL)) {
            gop_set_error(gop, GOP_ERROR_NOMEM, true);
            gop_emit_error(gop);
            return 1;
        }
    } else {
        gop->program_name = NULL;
    }
    return 0;
}
