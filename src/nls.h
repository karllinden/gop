/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef NLS_H
# define NLS_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# if ENABLE_NLS
#  if HAVE_LIBINTL_H
#   include <libintl.h>
#  endif /* HAVE_LIBINTL_H */
#  define _(string) dgettext(PACKAGE_DOMAIN, string)
#  define N_(string) (string)
# else /* !ENABLE_NLS */
#  define _(string) (string)
#  define N_(string) (string)
# endif /* !ENABLE_NLS */

#endif /* !NLS_H */
