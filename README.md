# Great Option Parsing

Author: Karl Linden

Report bugs to: https://gitlab.com/karllinden/gop/issues

Homepage: https://gitlab.com/karllinden/gop


## Introduction

Great Option Parsing (GOP) is an option parsing library written in C with a few
central design goals:

 * Modularity. A program should be able to have modules that may take command
   line arguments without the need to implement option parsing in main() or
   alike.
 * Sane defaults. It should be very easy and hassle free to use GOP for
   "conventional" option parsing. GOP should provide reasonable defaults so that
   the library user does not have to waste time writing numerous loops and
   checks that can be handled cleanly in GOP.
 * Simplicity. It should be easy to have complex argument parsing.
 * Coding standard. The library code should follow a strict coding standard. On
   a number of points GOP obeys the GNU coding standards. Furthermore it should
   be possible to easily write a program using GOP that complies with the GNU
   coding standards. Also, the library code should look good and consistent.
 * Adaptability. GOP should be able to be used in a wide range of option parsing
   constellations, not just simple "conventional" option parsing.
 * Nice output. The help and usage messages should be nicely formatted.
   This applies to all locales which GOP will support in the future.


## Motivation to write this library

This library is written as a replacement for the popt library. GOP is intended
to be superiour by design, for example by keeping a clean and consistent coding
style together with slotting to handle ABI changes.


## Features

GOP has many features. Firstly the minimum requirements for an option parsing
library:

 * parsing of long and short options,
 * easily created and and readable option tables,
 * nicely formatted automatic help and usage output.

Furthermore GOP has a number of additional features, such as:

 * Simple library usage using inversion of control. This means programs using
   GOP need no loops, gotos or jumps. Just call a few functions and GOP does the
   rest.
 * Possibility to hook up an option with a callback that will be invoked each
   time the option is encountered.
 * Parsing can be interrupted when a certain option is found. (This way the `--`
   option found in many programs or the `--args` option in `gdb` can be
   implemented.)
 * GOP has sophisticated error and exit handling to provide clean and common
   defaults (such as exiting on `--help` or `--usage`). Yet the library is still
   easy to use.
 * Native languages support using gettext (which can be turned off with
   `--no-nls`) and correct character width calculation using wide characters.
 * Possibility to parse options multiple times, so that programs that add
   more options after some initial information is gathered can be written
   simply. (For example a program could use modules with own command line
   options loaded at runtime from a path that can be modified on the command
   line or a build system that needs to know about all features and flags
   specified by the build system files before printing out help text). This is
   an example of how wide the application of GOP is. This also has some caveats,
   see below.


## Slots

To solve the ABI-problem GOP uses slots. A slot is the same as the API version
and by convention equals the major version of the library. In a slot the ABI
(and API) are guaranteed not to change in a backwards incompatible way. However,
between slots the ABI and API is likely to change. Multiple slots can be
installed on the same system at the same time.

This is the reason you cannot just check for the package gop in your pkgconfig
checks. Instead you should check for gop-${slot}, e.g. gop-4.

The library user chooses a slot to develop against and when he/she feels ready
to migrate to the next slot he/she just updates the slot in pkgconfig check,
updates the necessary code and that's it.

Since multiple slots can be installed at the same time there is no need to
keep some kind of holy backward or forward compatibility (of couree the API
should not be broken, just for the sake of breaking) at the same time
applications can be updated one by one.


## Installation

This library can be installed using:
```
./waf configure
./waf
./waf install
```

If you are building from git, you must, before the above commands, run:
```
git submodule update --init
```


## Documentation

To build and install documentation you need
[AsciiDoc](http://www.methods.co.nz/asciidoc/), [Graphviz](http://graphviz.org/)
(the `dot` program is sufficient) and
[source-highlight](https://www.gnu.org/software/src-highlite/source-highlight.htm).
If these are installed correctly and you append `--doc` to `./waf configure` the
documentation will be built and saved in `build/doc/reference-manual.html` when
you run `./waf` and installed into the desired documentation directory when you
run `./waf install`.


## Tests, documentation and examples

It is highly recommended that you test your GOP build, using the automated test
suite, before you start programming against the library. The independently
distributed tests are also good examples of how to use the GOP library, whence
reading them is recommended if you want crash course.

There is a small test suite in this distribution. It is not helpful as an
example of how to use the library, but instead useful for stress testing the
library. These stress tests can be run using:
```
./waf check
```
after having run `./waf`.

An exhaustive test and example suite is distributed separately as
[cotesdex](https://gitlab.com/karllinden/cotesdex) files in the package
[gop-cotesdex](https://gitlab.com/karllinden/gop-cotesdex). These are the
reasons to split the examples and tests from this distribution.

 1. Since the cotesdex program uses GOP to parse options, using cotesdex to
    generate tests would cause a circular dependency (which can be avoided by
    an `--test` option in waf, of course). A simple gop -> cotesdex ->
    gop-cotesdex dependency chain is cleaner.
 2. It is a cleaner approach when cross-compiling. When cross-compiling tests
    cannot be run when building the library. Instead tests should be run when
    the library is installed on the target architecture.
 3. The cotesdex dependency can be hard-wired in the test distribution.

Please report any test failures to the bug report address above.

## Supported Command Lines

In all the following extracts `prog` is the program that uses GOP, to parse its
command line options.

If `-a` is the short option corresponding to the long name `--a-option`, the
following two commands are equivalent:
```
prog -a
prog --a-option
```
The above two commands will result in an error if `-a` and `--a-option` expect
and argument. In this case the following are synonymous:
```
prog -a argument
prog -aargument
prog --a-option=argument
prog --a-option argument
```

If `-a` and `-b` are short options none of which expects an argument the
following are equivalent:
```
prog -a -b
prog -ab
```

If `-a` and `-b` are short options where only `-b` takes an argument then `arg`
will be interpreted as the argument to `-b` in the following:
```
prog -abarg
```

## Caveats of Multiple Parsing and Short Options

Unknown and erroneous short options break handling of an array of short options.
For example if the `-a` flag is unkown, but the `-b` flag is known, then the
latter is missed in the first round of parsing with the below construct
```
prog -ab
```
However, if the `-a` flag is added and the command line is parsed again, then
the above works as expected.

Note also that the above is ambigous if the short options `-a` that takes an
argument is added after the first parse. Then the above should mean that the
argument to `-a` is `b`. However, if GOP would have continued to parse the short
option array above, then the argument `b` to `-a` would have been interpreted as
the flag `-b` which is wrong.

Thus, when relying on multiple parsing be cautious about short options. One way
to avoid the above is to simply disallow adding short options after the first
round of parsing.
