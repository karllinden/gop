#!/usr/bin/env python
# encoding: utf-8
#
# This file is part of gop.
#
# Copyright (C) 2017-2018 Karl Linden
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

from __future__ import print_function

import os
import sys

import wafimport
from waflib import Build, Errors, Utils

VERSION = '4.0.1'
APPNAME = 'gop'

# these variables are mandatory ('/' are converted automatically)
top = '.'
out = 'build'

auto_options = []
extra_cflags = [
    '-fipa-pure-const',
    '-fstrict-overflow'
]
warning_cflags = [
    '-Wall',
    '-Wconversion',
    '-Winline',
    '-Wmissing-declarations',
    '-Wshadow',
    '-Wsign-compare',
    '-Wsign-conversion',
    '-Wstrict-overflow=5',
    '-Wsuggest-attribute=const',
    '-Wsuggest-attribute=pure',
    '-Wtype-limits',
    '-pedantic'
]

lib_sources = [
    'src/autohelp.c',
    'src/error.c',
    'src/exit.c',
    'src/gop.c',
    'src/parse.c',
    'src/program-name.c'
]

memblockswap_sources = [
    'src/memblockswap.c'
]

def options(opt):
    opt.load('compiler_c')
    opt.load('gnu_dirs')
    opt.load('waf_unit_test')

    opt.load('autooptions')
    opt.load('egypt')

    opt.set_auto_options_define('ENABLE_%s')
    opt.add_auto_option('shared-libs', 'Build shared libraries')
    opt.add_auto_option(
            'static-libs',
            'Build static libraries',
            default=False)

    opt.load('nls')

    doc = opt.add_auto_option(
            'doc',
            'Build and install documentation',
            default=False)
    doc.find_program('asciidoc')
    doc.find_program('dot')
    doc.find_program('source-highlight')

    tests = opt.add_auto_option('tests', 'Run tests on waf check')
    tests.find_program('python3')
    tests.find_program('valgrind')

def check_for_ioctl(conf):
    conf.check(header_name='sys/ioctl.h')

    fragment = \
        '#include <stdio.h>\n' \
        'int main(void) {\n' \
        '    fileno(stdout);\n' \
        '    return 0;' \
        '}\n'
    conf.check(
        msg='Checking for function fileno',
        fragment=fragment,
        define_name='HAVE_FILENO')

    fragment = \
        '#include <stdio.h>\n' \
        '#include <sys/ioctl.h>\n' \
        'int main(void) {\n' \
        '    struct winsize window_size;\n' \
        '    int fd = fileno(stdout);\n' \
        '    ioctl(fd, TIOCGWINSZ, &window_size);\n' \
        '    return 0;' \
        '}\n'
    conf.check(
        msg='Checking if TIOCGWINSZ works',
        fragment=fragment,
        define_name='HAVE_TIOCGWINSZ')

def configure(conf):
    conf.load('compiler_c')
    conf.load('gccdeps')
    conf.load('gnu_dirs')
    conf.load('waf_unit_test')

    conf.load('werror')

    conf.env['NLS_FUNCTIONS'] = [
        'dgettext',
        'bindtextdomain'
    ]
    conf.load('autooptions')
    conf.load('nls')
    conf.load('egypt')

    conf.env['UNLOCKED_FUNCTIONS'] = [
        'fileno',
        'fputc',
        'fputs'
    ]
    conf.load('unlocked')

    if not conf.env['SHARED_LIBS'] and not conf.env['STATIC_LIBS']:
        conf.fatal('At least one of --shared-libs and --static-libs is ' +
            'necessary')

    # POSIX.1-2008 compliant printf and wcwidth.
    conf.define('_POSIX_C_SOURCE', '200809L', quote=False)

    try:
        check_for_ioctl(conf)
    except Errors.ConfigurationError:
        pass

    conf.env.append_unique('CFLAGS', '-std=gnu99')
    conf.env.append_unique('CFLAGS', extra_cflags)
    conf.env.append_unique('CFLAGS', warning_cflags)

    conf.env['PACKAGE_NAME'] = APPNAME
    conf.env['PACKAGE_VERSION'] = conf.env['VERSION'] = VERSION
    conf.env['PACKAGE_URL'] = 'https://gitlab.com/karllinden/gop'
    conf.env['PACKAGE_BUGREPORT'] = conf.env['PACKAGE_URL'] + '/issues'
    conf.env['SLOT'] = VERSION.split('.')[0]
    conf.env['PACKAGE'] = APPNAME + '-' + conf.env['SLOT']

    # nls
    conf.env['DOMAIN'] = conf.env['PACKAGE']
    conf.env['POTFILES'] = lib_sources
    conf.env['LINGUAS'] = [
        'sv'
    ]
    conf.env['COPYRIGHT_HOLDER'] = \
            Utils.readf('AUTHORS').splitlines()[0]
    conf.env['MSGID_BUGS_ADDRESS'] = conf.env['PACKAGE_BUGREPORT']

    conf.define('PACKAGE_NAME', conf.env['PACKAGE_NAME'])
    conf.define('PACKAGE_DOMAIN', conf.env['DOMAIN'])
    conf.define('LOCALEDIR', conf.env['LOCALEDIR'])

    conf.env['PKGCONFIGDIR'] = os.path.join(
            conf.env['LIBDIR'],
            'pkgconfig')
    conf.env['SLOTINCLUDEDIR'] = os.path.join(
            conf.env['INCLUDEDIR'],
            conf.env['PACKAGE'])

    install_dirs = [
        ('PREFIX',      'Install prefix'),
        ('EXEC_PREFIX', 'Installation prefix for binaries'),
        ('LIBDIR',      'Library directory'),
        ('INCLUDEDIR',  'Header directory'),
        ('LOCALEDIR',   'Locale data dir'),
        ('DOCDIR',      'Documentation directory'),
    ]

    print()
    print('General configuration')
    conf.summarize_auto_options()
    print()
    print('Install directories')
    for name,descr in install_dirs:
        conf.msg(descr, conf.env[name], color='CYAN')

    conf.write_config_header('config.h')

def build(bld):
    bld.load('werror')

    bld.env.append_unique('CFLAGS', '-DHAVE_CONFIG_H=1')
    bld.env['INCLUDES'] = [bld.bldnode.abspath()]

    bld.load('egypt')

    # Build the memblockswap static library. Needed for the library, but also
    # unit tested, so therefore it is saved in a USE variable.
    bld(
        features     = 'c cstlib',
        target       = 'memblockswap',
        source       = memblockswap_sources,
        install_path = None
    )
    bld.env['INCLUDES_MEMBLOCKSWAP'] = [bld.srcnode.find_dir('./src')]
    bld.env['STLIBPATH_MEMBLOCKSWAP'] = [bld.bldnode]
    bld.env['STLIB_MEMBLOCKSWAP'] = ['memblockswap']

    # Declare use variables for GOP. This is independent of where it is used, so
    # that it can be used.
    bld.env['INCLUDES_GOP'] = [bld.srcnode.find_dir('./include')]

    if bld.env['STATIC_LIBS']:
        bld.env['STLIBPATH_GOP'] = [bld.bldnode]
        bld.env['STLIB_GOP'] = [bld.env['PACKAGE']]

        # Add the memblockswap sources to the static library to avoid the need
        # to install libmemblockswap.a.
        bld(
            features     = 'c cstlib',
            target       = bld.env['PACKAGE'],
            name         = 'static-lib',
            source       = lib_sources + memblockswap_sources,
            includes     = bld.env['INCLUDES_GOP'],
            use          = ['INTL'],
            install_path = bld.env['LIBDIR']
        )

    if bld.env['SHARED_LIBS']:
        # Override the library use variables for GOP, so that the shared library
        # is preferred.
        del bld.env['STLIBPATH_GOP'], bld.env['STLIB_GOP']
        bld.env['LIBPATH_GOP'] = [bld.bldnode]
        bld.env['LIB_GOP'] = [bld.env['PACKAGE']]

        lib_gop = bld(
            features     = 'c cshlib',
            target       = bld.env['PACKAGE'],
            name         = 'shared-lib',
            source       = lib_sources,
            includes     = bld.env['INCLUDES_GOP'],
            use          = ['INTL', 'MEMBLOCKSWAP'],
            install_path = bld.env['LIBDIR']
        )

    headers = [
        'include/gop-autohelp.h',
        'include/gop-error.h',
        'include/gop-exit.h',
        'include/gop-general.h',
        'include/gop.h'
    ]

    bld.install_files(bld.env['SLOTINCLUDEDIR'], headers)

    bld(
        features     = 'subst',
        source       = 'gop.pc.in',
        target       = 'gop-%s.pc' % bld.env['SLOT'],
        install_path = bld.env['PKGCONFIGDIR']
    )

    if bld.env['DOC']:
        bld(
            source       = 'doc/flow.gv',
            target       = 'doc/flow.svg',
            rule         = '${DOT} -Tsvg -o ${TGT} ${SRC}',
            install_path = bld.env['DOCDIR']
        )
        bld(
            source       = 'doc/reference-manual.txt',
            target       = 'doc/reference-manual.html',
            rule         = '${ASCIIDOC} --attribute toc -o ${TGT} ${SRC}',
            install_path = bld.env['DOCDIR']
        )

    bld.load('nls')

    if bld.env['ENABLE_EGYPT']:
        bld(
            features = 'egypt',
            source   = lib_sources,
            target   = 'gop.svg',
            includes = ['./include', './src']
        )

def check(bld):
    # To run the tests specific programs are needed, so if those do not exist of
    # --no-tests is given, then check should do nothing.
    if not bld.env['TESTS']:
        return

    bld.load('waf_unit_test')

    # Because build is a prerequisite to check, run that now. This also exposes
    # the use variables to the tests.
    fun = bld.fun
    bld.fun = 'build'
    build(bld)
    bld.fun = fun

    bld.recurse('tests')

class CheckClass(Build.BuildContext):
    cmd = 'check'
    fun = 'check'

def update_po(bld):
    bld.load('nls')
