/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _GOP_GENERAL_H_
# define _GOP_GENERAL_H_

# include <stdio.h>

# undef GOP_BEGIN_C_DECLS
# undef GOP_END_C_DECLS
# ifdef __cplusplus
#  define GOP_BEGIN_C_DECLS extern "C" {
#  define GOP_END_C_DECLS }
# else
#  define GOP_BEGIN_C_DECLS /* empty */
#  define GOP_END_C_DECLS /* empty */
# endif

GOP_BEGIN_C_DECLS

enum gop_return_e {
    /*
     * If a function returns this it wants gop to continue with what it
     * was doing.
     */
    GOP_DO_CONTINUE = 0,

    /*
     * This return value should never be used in a consumer program. It
     * is only used internally within the library to notify the caller
     * that an error occured.
     */
    GOP_DO_ERROR,

    /* If a function wants the program to exit, this value is returned. */
    GOP_DO_EXIT,

    /*
     * A function can return this value to tell that the program wants
     * to regain control and thus wants the gop subroutine to return.
     */
    GOP_DO_RETURN
};
typedef enum gop_return_e gop_return_t;

/* This is the argument type enum. */
enum gop_type_e {
    /* double */
    GOP_DOUBLE,

    /* The option is the last in the option list. */
    GOP_END,

    /* float */
    GOP_FLOAT,

    /* int */
    GOP_INT,

    /* long double */
    GOP_LONG_DOUBLE,

    /* long int */
    GOP_LONG_INT,

    /* long long int */
    GOP_LONG_LONG_INT,

    /*
     * The option takes no argument but argument pointer may point to an
     * int that is used to show whether the option was given. Otherwise
     * it should be NULL.
     */
    GOP_NONE,

    /* short */
    GOP_SHORT,

    /* char * */
    GOP_STRING,

    /* unsigned int */
    GOP_UNSIGNED_INT,

    /* unsigned long int */
    GOP_UNSIGNED_LONG_INT,

    /* unsigned long long int */
    GOP_UNSIGNED_LONG_LONG_INT,

    /* unsigned short */
    GOP_UNSIGNED_SHORT,

    /*
     * the argument is either a 'yes' or a 'no' and the argument pointer
     * points to an int that will be filled in with 1 if 'yes' was given
     * or 0 if 'no' was given.
     */
    GOP_YESNO
};
typedef enum gop_type_e gop_type_t;

typedef void * gop_malloc_callback_t (size_t size);
typedef void   gop_free_callback_t   (void * ptr);
typedef void * gop_realloc_callback_t(void * ptr, size_t size);
typedef char * gop_strdup_callback_t (const char * str);

/* Opaque definition of the gop_t type. */
typedef struct gop_s gop_t;

typedef gop_return_t gop_callback_t (gop_t *);

typedef void gop_arg_destructor_t (char *);

struct gop_option_s {
    const char * long_name;
    char short_name;
    gop_type_t argument_type;
    void * argument;
    gop_callback_t * callback;
    const char * description;
    const char * argument_description;
};
typedef struct gop_option_s gop_option_t;

# define GOP_TABLEEND {NULL, '\0', GOP_END, NULL, NULL, NULL, NULL}

gop_t * gop_new(void);
gop_t * gop_new_mem(gop_malloc_callback_t * malloc,
                    gop_free_callback_t * free,
                    gop_realloc_callback_t * realloc,
                    gop_strdup_callback_t * strdup)
    __attribute__((nonnull));
void gop_destroy(gop_t * gop);

int gop_init_nls(void);

int gop_add_table_with_domain(gop_t * gop,
                              const char * name,
                              const gop_option_t * options,
                              const char * domain)
    __attribute__((nonnull(1,3)));

int gop_add_table(gop_t * gop,
                  const char * name,
                  const gop_option_t * options)
    __attribute__((nonnull(1,3)));

int gop_parse(gop_t * gop, int * argcp, char ** argv)
    __attribute__((nonnull));

int gop_set_program_name(gop_t * gop, const char * program_name)
    __attribute__((nonnull(1)));

void gop_set_arg_destructor(gop_t * gop,
                            gop_arg_destructor_t * arg_destructor)
    __attribute__((nonnull(1)));

void * gop_get_pointer(const gop_t * gop)
    __attribute__((nonnull));
void gop_set_pointer(gop_t * gop, void * pointer)
    __attribute__((nonnull(1)));

void gop_errfile(gop_t * gop, FILE * errfile)
    __attribute__((nonnull));

void gop_outfile(gop_t * gop, FILE * outfile)
    __attribute__((nonnull));

GOP_END_C_DECLS

#endif /* _GOP_GENERAL_H_ */
