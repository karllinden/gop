/*
 * This file is part of gop.
 *
 * Copyright (C) 2014-2015, 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _GOP_ERROR_H_
# define _GOP_ERROR_H_

# include <gop-general.h>

GOP_BEGIN_C_DECLS

/* Different types of error that gop can encounter. */
enum gop_error_e {
    GOP_ERROR_CONV, /* Error converting an argument string to requested type. */
    GOP_ERROR_EXPARG, /* Expected an argument. */
    GOP_ERROR_INVRET, /* Invalid return value. */
    GOP_ERROR_NOMEM, /* Insufficient memory. */
    GOP_ERROR_NONE, /* No error. */
    GOP_ERROR_PRINT, /* Printing failed. */
    GOP_ERROR_TERMWIDTH, /* The terminal is not wide enough. */

    /*
     * Raised when an option that does not allow an argument (it is of type
     * GOP_NONE) gets an argument in this form:
     * --option-that-does-not-take-an-argument=unexpected-argument.
     */
    GOP_ERROR_UNEXARG, /* Unexpected argument. */

    GOP_ERROR_UNKLOPT, /* Unknown long option. */
    GOP_ERROR_UNKSOPT, /* Unknown short option. */
    GOP_ERROR_UNKTYPE, /* Unknown argument type. */

    /*
     * An option of type GOP_YESNO was given some argument neither 'yes' nor
     * 'no'.
     */
    GOP_ERROR_YESNO
};
typedef enum gop_error_e gop_error_t;

/*
 * Some errors supply additional information. This information can be used to
 * give better error reporting.
 */
union gop_error_data_u {
    struct {
        const gop_option_t * option;
        const char * argument;
    } conv;

    struct {
        const gop_option_t * option;
    } exparg;

    struct {
        gop_return_t value;
    } invret;

    struct {
        const gop_option_t * option;
        const char * argument;
    } unexarg;

    struct {
        const char * long_option;
    } unklopt;

    struct {
        char short_option;
    } unksopt;

    struct {
        const gop_option_t * option;
        const char * argument;
    } yesno;
};
typedef union gop_error_data_u gop_error_data_t;

void gop_aterror(gop_t * gop, gop_callback_t * callback)
    __attribute__((nonnull(1)));

gop_return_t gop_default_error_handler(const gop_t * gop)
    __attribute__((nonnull));

gop_error_t gop_get_error(const gop_t * gop)
    __attribute__((nonnull, pure));

const gop_error_data_t * gop_get_error_data(const gop_t * gop)
    __attribute__((const, nonnull));

int gop_get_errno_set(const gop_t * gop)
    __attribute__((nonnull, pure));

void gop_print_error(const gop_t * gop)
    __attribute__((nonnull));

const char * gop_strerror(gop_error_t error)
    __attribute__((const, returns_nonnull));

GOP_END_C_DECLS

#endif /* _GOP_ERROR_H_ */
