#!/usr/bin/env python
# encoding: utf-8
#
# This file is part of gop.
#
# Copyright (C) 2017-2018 Karl Linden
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

import argparse
import os
import random
import select
import subprocess
import sys

class Main:
    def __init__(self):
        parser = argparse.ArgumentParser(description='Run mestress tests')
        parser.add_argument(
                'memstress',
                metavar='MEMSTRESS',
                type=str,
                help='the memstress program')
        parser.add_argument(
                '--jobs',
                '-j',
                type=int,
                default=1,
                help='number of jobs to run in parallel')
        parser.add_argument(
                '--n-tests',
                '-n',
                type=int,
                default=100,
                help='number of tests to run')
        self.args = parser.parse_args()

        # Valgrind expects a path to the binary as argument, otherwise it starts
        # to look in PATH where the executable typically cannot be found.
        self.memstress = os.path.abspath(self.args.memstress)

        # Pass valgrind through the environment to avoid shell escaping
        # problems. Recall that this script is invoked from the build system
        # which does not make escaping simpler.
        self.valgrind = self.get_env_var('VALGRIND', '/usr/bin/valgrind')

        self.n_procs = self.args.n_tests
        self.poller = select.epoll()
        self.procs = {}

    def run(self):
        fail_count = 0

        for j in range(self.args.jobs):
            self.launch_process()

        while len(self.procs) > 0:
            for fd, flags in self.poller.poll():
                self.poller.unregister(fd)

                # Launch a new process if there are more processes that replaces
                # the finished process. Note that this does not launch a new
                # process if no more processes should be started.
                self.launch_process()

                # Check the finished process for failure.
                p = self.procs.pop(fd)
                out, err = p.communicate()
                if p.returncode != 0:
                    fail_count += 1
                    print('STDOUT:')
                    print(out.decode('utf-8'))
                    print('STDERR:')
                    print(err.decode('utf-8'))

        return fail_count

    def get_env_var(self, var, default):
        try:
            return os.environ[var]
        except KeyError:
            return default

    def launch_process(self):
        if self.n_procs <= 0:
            return
        self.n_procs -= 1

        args = [
            self.valgrind,
            '--error-exitcode=63',
            '--leak-check=full',
            '--show-leak-kinds=all',
            self.memstress
        ]

        # Seed the programs random number generator with a randomized seed from
        # this script so that no no randomness is lost due to poor seeding in
        # the program. Also this makes it easy to print diagnostics before
        # exiting. The seed must be an unsigned integer. If this were to be
        # portable a check in the build system should be done.
        os.environ['SEED'] = str(random.randint(0, 2**32-1))

        # Launch the process and register it in the poller and the dict.
        p = subprocess.Popen(
                args,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
        self.procs[p.stdout.fileno()] = p
        self.poller.register(p.stdout)

main = Main()
sys.exit(main.run())
