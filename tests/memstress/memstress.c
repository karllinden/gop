/*
 * This file is part of gop.
 *
 * Copyright (C) 2017-2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gop.h>

/*
 * This is the success rate of the memory allocations. For example a value of
 * 0.7 means that 70% of the memory allocations should succeed.
 */
#define SUCCESS_RATE .7

/*
 * This is the offset in bytes of the returned memory blocks relative to the
 * actually allocated memory blocks. This is used to catch mismatches between
 * the supplied memory functions and the standard ones. This can reveal bugs
 * where GOP uses standard memory functions where it should use the custom
 * functions instead.
 */
#define OFFSET 8

static void
seed(void)
{
    const char * s = getenv("SEED");
    if (s != NULL) {
        long l = 0;
        l = atol(s);
        unsigned u = (unsigned)l;
        srand(u);
        printf("Seeding with %u.\n", u);
    }
}

static int
should_succeed()
{
    int r = rand();
    return r < RAND_MAX * SUCCESS_RATE;
}

static void *
mymalloc(size_t size)
{
    if (should_succeed()) {
        char * mem = malloc(size + OFFSET);
        if (mem != NULL) {
            return mem + OFFSET;
        }
    }
    errno = ENOMEM;
    return NULL;
}

static void
myfree(void * ptr)
{
    if (ptr != NULL) {
        char * mem_plus_offset = ptr;
        free(mem_plus_offset - OFFSET);
    }
}

static void *
myrealloc(void * ptr, size_t size)
{
    if (ptr == NULL) {
        return mymalloc(size);
    } else if (size == 0) {
        myfree(ptr);
        return NULL;
    } else if (should_succeed()) {
        char * mem_plus_offset = ptr;
        char * mem = mem_plus_offset - OFFSET;
        char * newmem = realloc(mem, size + OFFSET);
        if (newmem != NULL) {
            return newmem + OFFSET;
        }
    }
    errno = ENOMEM;
    return NULL;
}

static char *
mystrdup(const char * str)
{
    size_t size = strlen(str) + 1;
    char * copy = mymalloc(size);
    if (copy != NULL) {
        memcpy(copy, str, size);
    }
    return copy;
}

/*
 * A memory failure should not make the GOP context corrupt. Thus, it should be
 * safe to continue whenever a memory allocation error occurs.
 */
static gop_return_t
atexit_cb_gop(gop_t * gop)
{
    return GOP_DO_RETURN;
}

int
main(int argc, char ** argv)
{
    gop_t * gop;

    seed();

    /* Create a new GOP context with the broken memory functions. */
    gop = gop_new_mem(&mymalloc, &myfree, &myrealloc, &mystrdup);
    if (gop == NULL) {
        goto error;
    }

    /* Make GOP return instead of exiting. */
    gop_atexit(gop, &atexit_cb_gop);

    /* Call some functions that are likely to allocate memory. */
    gop_description(gop, "A program to test for memory errors");
    gop_extra_help(gop, "Run this program and see what happens.");
    gop_add_usage(gop, "[OPTIONS...]");
    gop_add_usage(gop, "[OPTIONS...]");

    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);

    gop_destroy(gop);

error:
    /*
     * Exit successfully unconditionally because errors are expected. Valgrind
     * will detect memory errors, so the exit code is useless anyway.
     */
    return EXIT_SUCCESS;
}
