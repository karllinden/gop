/*
 * This file is part of gop.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <stdlib.h>

#include <gop.h>

/* Exit code to exit with if the terminal width is insufficient. */
#define INSUFFICIENT_TERMWIDTH_EXITCODE 63

static gop_return_t
aterror_cb(gop_t * gop)
{
    gop_error_t error = gop_get_error(gop);
    if (error == GOP_ERROR_TERMWIDTH) {
        /* Exit with the given exit code. */
        gop_set_exit_status(gop, INSUFFICIENT_TERMWIDTH_EXITCODE);
        return GOP_DO_EXIT;
    }

    /* Invoke the default error handler. */
    return GOP_DO_ERROR;
}

int
main(int argc, char * argv[])
{
    gop_t * gop;

    char * string         = NULL;
    int    obvious        = -1;
    int    n              = 0;
    long   lng            = 0L;
    int    print_exitcode = 0;

    const gop_option_t options[] = {
        /* An option with both a long and short name. */
        {"string", 's', GOP_STRING, &string, NULL, "give a string", "STRING"},

        /*
         * An option that obviously needs neither a description nor a short
         * name.
         */
        {"obvious", '\0', GOP_YESNO, &obvious, NULL, NULL, NULL},

        /* An option without a long name. */
        {NULL, 'n', GOP_INT, &n, NULL, "an integer", "INTEGER"},

        /* An option not only whose type is long, but with long description. */
        {"long", '\0', GOP_LONG_INT, &lng, NULL,
            "a very long description for this long option, which in fact does "
                "nothing",
            "LONG_INTEGER_THAT_DOES_NOTHING"},

        {"print-exitcode", '\0', GOP_NONE, &print_exitcode, NULL,
            "print the exitcode that this program exits with in case of "
                "insufficient terminal width",
            NULL},

        GOP_TABLEEND
    };

    gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }
    gop_add_table(gop, "Program options", options);
    gop_autohelp(gop);

    /*
     * Register the custom error handler that sets the exit status when there is
     * insufficient terminal width.
     */
    gop_aterror(gop, &aterror_cb);

    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    /* Print exitcode if that has been requested. */
    if (print_exitcode) {
        printf("%d\n", INSUFFICIENT_TERMWIDTH_EXITCODE);
    }

    return 0;
}
