#!/usr/bin/env python
# encoding: utf-8
#
# This file is part of gop.
#
# Copyright (C) 2018 Karl Linden
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

# This test script invokes the termwidth program with smaller and smaller
# terminal widths checking that the output does not overflow.

import os
import subprocess
import sys

if len(sys.argv) != 2:
    print('Usage: termwidth.py TERMWIDTH_PROGRAM')
    sys.exit(1)
program = os.path.abspath(sys.argv[1])

# Get the exitcode that the program uses for insufficient terminal width. This
# is needed to know when to stop testing.
out = subprocess.check_output([program, '--print-exitcode'])
insufficient_termwidth_exitcode = int(out.decode('utf-8'))
del out

def print_error(out, index, columns):
    lines = out.splitlines()
    ll = len(lines[index])

    error = 'Line {} overflowed. It is {} characters long but the terminal ' + \
            'width is only {}.'
    print(error.format(index, ll, columns))
    print('The output is printed below with * marking terminal width.')

    # Pretty print a header with the column numbers.
    il = len(str(len(lines))) + 2
    print(il * ' ', end='')
    for x in range(1, ll+1):
        if x == columns:
            c = '*'
        elif x % 10 == 0:
            c = x // 10
        else:
            c = ' '
        print(c, end='')
    print()
    print(il * ' ', end='')
    for x in range(1, ll+1):
        if x == columns:
            c = '*'
        else:
            c = x % 10
        print(c, end='')
    print()

    for index, line in enumerate(lines):
        print(str(index + 1).rjust(il - 2) + ': ' + line)
    sys.exit(1)


# Combination of arguments to test. Only the help printing arguments are
# necessary here, because it is the output of those that is tested.
args_combinations = [
        ['--help'],
        ['--usage']
]

# Test with number of columns decreasing until the program says the terminal
# width is insufficient.
columns = 120
while columns >= 0:
    os.environ['COLUMNS'] = str(columns)

    for args in args_combinations:
        p = subprocess.Popen(
                [program] + args,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
        out, err = [x.decode('utf-8') for x in p.communicate()]

        if p.returncode == insufficient_termwidth_exitcode:
            # If there was insufficient terminal width, then reducing the
            # columns will not make the situation better. Thus, do not try this
            # option with smaller values of COLUMNS.
            args_combinations.remove(args)
            continue
        elif p.returncode != 0:
            # The program should exit with a succesful status (0), so print a
            # summary and bail out if it does not.
            print('Unsuccessful exit status:')
            print('STATUS:', p.returncode)
            print('STDOUT:')
            print(out)
            print('STDERR:')
            print(err)
            sys.exit(1)

        # The program succeeded, so check that it did not overflow the terminal
        # width.
        for index, line in enumerate(out.splitlines()):
            if len(line) > columns:
                print_error(out, index, columns)

    columns -= 1

sys.exit(0)
