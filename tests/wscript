#!/usr/bin/env python
# encoding: utf-8
#
# This file is part of gop.
#
# Copyright (C) 2017-2018 Karl Linden
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import os

from waflib.Tools import waf_unit_test

def append_to_ld_library_path(bld):
    # The tests require the library, so in case the library is shared the
    # library must be on LD_LIBRARY_PATH.
    old_ld_library_path = os.environ.get('LD_LIBRARY_PATH', '')
    paths = old_ld_library_path.split(':')

    # The empty string should never be in LD_LIBRARY_PATH. The empty string will
    # be introduced in case LD_LIBRARY_PATH is empty as the above default is.
    while True:
        try:
            paths.remove('')
        except ValueError:
            break

    paths.extend([str(x) for x in bld.env['LIBPATH_GOP']])
    os.environ['LD_LIBRARY_PATH'] = ':'.join(paths)

def check(bld):
    bld.add_post_fun(waf_unit_test.summary)
    bld.add_post_fun(waf_unit_test.set_exit_code)

    append_to_ld_library_path(bld)

    for x in ['memblockswap', 'memstress', 'termwidth']:
        bld.recurse(x)
