/*
 * This file is part of gop.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdio.h>
#include <string.h>

#include "memblockswap.h"

#define N_CHARS_MAX 25

typedef int test_func_t();

static char str[N_CHARS_MAX + 1];

static void
genstr(unsigned len)
{
    for (unsigned i = 0; i < len; ++i) {
        str[i] = (char)('a' + i);
    }
    str[len] = '\0';
}

static int
expect(char * expected)
{
    if (strcmp(str, expected) == 0) {
        return 0;
    } else {
        fprintf(stderr, "error: got %s but expected %s\n", str, expected);
        return 1;
    }
}

static int
test1()
{
    genstr(6);
    memblockswap(str, 3, 3);
    return expect("defabc");
}

static int
test2()
{
    genstr(5);
    memblockswap(str, 3, 2);
    return expect("deabc");
}

static int
test3()
{
    genstr(5);
    memblockswap(str, 2, 3);
    return expect("cdeab");
}

static int
test4()
{
    genstr(7);
    memblockswap(str, 5, 2);
    return expect("fgabcde");
}

static int
test_inverse()
{
    int retval = 0;
    char expected[N_CHARS_MAX + 1];

    for (unsigned n = 0; n <= N_CHARS_MAX; ++n) {
        genstr(n);
        memcpy(expected, str, n + 1);
        for (unsigned i = 0; i <= n; ++i) {
            unsigned j = n - i;
            memblockswap(str, i, j);
            memblockswap(str, j, i);
        }
        retval += expect(expected);
    }

    return retval;
}

int
main(void)
{
    int exit_status = 0;

    test_func_t * tests[] = {
        &test1,
        &test2,
        &test3,
        &test4,
        &test_inverse,
        NULL
    };
    for (size_t i = 0; tests[i] != NULL; ++i) {
        exit_status += (*tests[i])();
    }

    return exit_status;
}
