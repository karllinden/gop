# Changelog
All notable changes to this project will be documented in this file.

The format is based on
[Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project
adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [4.0.0]
### Changed
 - The program can pass a function to be called on each argument of argv before
   the library removes the argument. This function is called an argument
   destructor and can be set with `gop_set_arg_destructor`.
 - GNU-style short options are supported. It is now possible to write -farg
   where -f takes an argument (arg).
 - The way short options are handled has been changed. The library now stops
   handling an array of short options as soon as an error occurs, instead of
   trying to parser more options in the same argv entry.
 - As a result of the above change, `GOP_ERROR_COMPSHRTEXPARG` has been dropped.
 - The library now consistently applies the following policy: if an option has
   been parsed by the library and the program has been notified for example by a
   value that has been updated or a callback that has been called, then the
   the option and its argument, if any, is removed from argv and not saved in
   the modified argv. As a result the behaviour when a callback returns
   GOP_DO_RETURN has been changed.
 - The new context creation function `gop_new_mem` has been implemented, which
   allows programs to specify which memory allocation functions the library
   should use.
 - `GOP_DO_RETURN_WO_HANDLER` has been dropped in favour of `GOP_DO_CONTINUE`.
   `GOP_DO_ERROR` is now a valid return value from an error handler and it does
   what `GOP_DO_CONTINUE` did before, i.e. it issues the library error handler.
 - Bugfixes:
    - An infinite loop has been fixed.
    - Some overflows on tiny terminals have been fixed.
    - The libray no longer leaks memory if realloc fails.
 - The project is now maintained at
   [gitlab](https://gitlab.com/karllinden/gop).
 - The project uses waf as build system.
 - The old ChangeLog has been pruned in favor this file.

[Unreleased]: https://gitlab.com/karllinden/gop/compare/v4.0.0...HEAD
[4.0.0]: https://gitlab.com/karllinden/gop/compare/v3.1.1...v4.0.0
